<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Person::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phones' => '[]',
        'emails' => '[]',
        'relationships' => '[]',
        'others' => '[]',
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'code' => $faker->unique()->company,
        'customer_group_id' => 1,
    ];
});

$factory->define(App\CustomerGroup::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'code' => $faker->unique()->randomNumber(5, true),
    ];
});

$factory->define(App\Driver::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
    ];
});

$factory->define(App\Car::class, function (Faker\Generator $faker) {
    $car = config('myfleet.cars')[array_rand(config('myfleet.cars'))];
    return [
        'type' => $car['type'],
        'class' => $car['classes'][array_rand($car['classes'])],
        'make' => 'Toyota',
        'model' => 'Camry',
        'fuel' => ['Gas', 'Diesel'][array_rand(['Gas', 'Diesel'])],
        'tx' => ['AT', 'MT'][array_rand(['AT', 'MT'])],
        'year' => $faker->year,
        'color' => $faker->colorName,
        'max_cap' => random_int(2, 10),
        'engine_no' => str_random(10),
        'serial_no' => str_random(10),
        'or_no' => str_random(10),
        'cr_no' => str_random(10),
        'conduction_no' => str_random(10),
        'registered_owner' => $faker->name,
        'status' => 'Active',
    ];
});

$factory->define(App\Reservation::class, function (Faker\Generator $faker) {
//    dd($faker->dateTimeThisYear->format('Y-m-d H:i'));
    return [
        'date' => $faker->date(),
        'department' => $faker->word,
        'purpose' => $faker->word,
        'charging_loc' => $faker->word,
        'trip_date' => $faker->date(),
        'type' => config('myfleet.trip_types')[array_rand(config('myfleet.trip_types'))],
        'location' => config('myfleet.trip_locations')[array_rand(config('myfleet.trip_locations'))],
        'vehicle' => $faker->word,
        'pickup_place' => $faker->word,
        'pickup_time' => $faker->time(),
        'destination' => $faker->word,
        'trip_start' => $faker->dateTimeThisYear->format('Y-m-d H:i'),
        'trip_end' => $faker->dateTimeThisYear->format('Y-m-d H:i'),
        'notes' => $faker->paragraph,
        'customer_id' => 1,
        'requestor_id' => 1,
        'authorizer_id' => 1,
        'passenger_id' => 1,
        'passenger_contact' => '123123',
        'ref_no' => '123',
        'po_document_path' => null,
    ];
});

$factory->define(App\Trip::class, function (Faker\Generator $faker) {

    return [
        'completed' => true,
        'ticket_no' => $faker->word,
        'date' => $faker->date(),
        'type' => $faker->word,
        'location' => $faker->word,
        'destination' => $faker->word,
        'pickup_place' => $faker->word,
        'petty_cash' => 100,
        'passenger_contact' => $faker->word,
        'pickup_time' => $faker->time(),
        'notes' => $faker->word,
        'car_id' => 1,
        'driver_id' => 1,
        'reservation_id' => 1,
        'passenger_id' => 1,
    ];
});

$factory->define(App\Payment::class, function (Faker\Generator $faker) {

    return [
        'ref_no' => $faker->unique()->randomNumber(5, true),
        'date' => $faker->date,
        'amount' => 100,
        'customer_id' => 1,
        'statement_id' => 1,
        'receipt_id' => null,
    ];
});

$factory->define(App\Statement::class, function (Faker\Generator $faker) {

    return [
        'ref_no' => $faker->unique()->randomNumber(5, true),
        'date' => $faker->date,
        'total' => 100,
        'customer_id' => 1,
        'transmittal_id' => null,
    ];
});