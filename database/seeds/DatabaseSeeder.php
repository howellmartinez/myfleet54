<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);

//        factory(App\Customer::class, 20)->create()->each(function ($customer) {
//            $customer->people()->save(factory(App\Person::class)->make());
//        });
//
//        factory(App\Driver::class, 5)->create();
        factory(App\Car::class, 6)->create();
//        factory(App\Reservation::class, 6)->create();
//
//        factory(App\Trip::class, 2)->create();
//
//        factory(App\Statement::class, 5)->create();
//
//        factory(App\Payment::class, 100)->create();

        //
        Permission::create(['name' => 'create customers']);
        Permission::create(['name' => 'edit customers']);
        Permission::create(['name' => 'view customers']);
        Permission::create(['name' => 'create suppliers']);
        Permission::create(['name' => 'edit suppliers']);
        Permission::create(['name' => 'view suppliers']);

        App\User::find(1)->givePermissionTo('create customers');

        factory(App\CustomerGroup::class, 20)->create();
        factory(App\Customer::class, 20)->create();
    }
}
