<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type');
            $table->string('class');
            $table->string('make');
            $table->string('model');
            $table->string('fuel');
            $table->string('tx');
            $table->string('year');
            $table->string('color');
            $table->string('max_cap');

            $table->string('plate')->nullable();
            $table->string('conduction_no');
            $table->string('engine_no');
            $table->string('serial_no');
            $table->string('or_no');
            $table->string('cr_no');
            $table->string('registered_owner');
            $table->string('status');

            $table->string('updated_by')->default('System');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
