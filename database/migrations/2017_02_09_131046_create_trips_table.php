<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket_no')->unique();
            $table->boolean('completed')->default(false);

            // Initial value copied over from reservation
            $table->date('date');
            $table->string('type');
            $table->string('location');
            $table->string('destination');
            $table->string('pickup_place');
            $table->time('pickup_time');
            $table->string('passenger_contact')->nullable();

            $table->decimal('petty_cash', 15, 2)->default(0.00);
            $table->text('notes')->nullable();

            $table->integer('reservation_id')->unsigned();
            $table->foreign('reservation_id')->references('id')->on('reservations');

            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('drivers');

            $table->integer('car_id')->unsigned();
            $table->foreign('car_id')->references('id')->on('cars');

            // Copied over from reservation
            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('people');

            $table->string('updated_by')->default('System');
            $table->timestamps();

            // Billing

//            $table->string('bill_no')->nullable();
//
//            $table->decimal('regular_fare', 15, 2)->default(0.00);
//            $table->decimal('excess_hours_charge', 15, 2)->default(0.00);
//            $table->decimal('driver_ot_charge', 15, 2)->default(0.00);
//            $table->decimal('vatable_total', 15, 2)->default(0.00);
//            $table->decimal('vat', 15, 2)->default(0.00);
//            $table->decimal('service_total', 15, 2)->default(0.00);
//
//            $table->decimal('fuel_charge', 15, 2)->default(0.00);
//            $table->decimal('meals', 15, 2)->default(0.00);
//            $table->decimal('tolls', 15, 2)->default(0.00);
//            $table->decimal('parking', 15, 2)->default(0.00);
//            $table->decimal('lodging', 15, 2)->default(0.00);
//            $table->decimal('others', 15, 2)->default(0.00);
//            $table->decimal('reimbursement_total', 15, 2)->default(0.00);
//
//            $table->decimal('total', 15, 2)->default(0.00);
//
//            $table->integer('trip_id')->unsigned();
//            $table->foreign('trip_id')->references('id')->on('trips');
//
//            $table->integer('statement_id')->unsigned()->nullable();
//            $table->foreign('statement_id')->references('id')->on('statements');
//
//            $table->string('approved_by');
//
//            $table->string('updated_by')->default('System');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
