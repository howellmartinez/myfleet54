<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');

            // TODO non nullable ref_no?
            $table->string('ref_no')->nullable();

            $table->date('date');

            $table->decimal('regular_fare', 15, 2)->default(0.00);
            $table->decimal('excess_hours_charge', 15, 2)->default(0.00);
            $table->decimal('driver_ot_charge', 15, 2)->default(0.00);
            $table->decimal('vatable_total', 15, 2)->default(0.00);
            $table->decimal('vat', 15, 2)->default(0.00);
            $table->decimal('service_total', 15, 2)->default(0.00);

            $table->decimal('fuel_charge', 15, 2)->default(0.00);
            $table->decimal('meals', 15, 2)->default(0.00);
            $table->decimal('tolls', 15, 2)->default(0.00);
            $table->decimal('parking', 15, 2)->default(0.00);
            $table->decimal('lodging', 15, 2)->default(0.00);
            $table->decimal('others', 15, 2)->default(0.00);
            $table->decimal('reimbursement_total', 15, 2)->default(0.00);

            $table->decimal('total', 15, 2)->default(0.00);

            $table->integer('trip_id')->unsigned();
            $table->foreign('trip_id')->references('id')->on('trips');

            $table->integer('statement_id')->unsigned()->nullable();
            $table->foreign('statement_id')->references('id')->on('statements');

            $table->string('approved_by');

            $table->string('updated_by')->default('System');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
