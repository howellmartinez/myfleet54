<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order');

            $table->string('origin');
            $table->string('destination');

            $table->dateTime('datetime_start');
            $table->dateTime('datetime_end');

            $table->integer('km_start');
            $table->integer('km_end');

            $table->integer('fuel_start');
            $table->integer('fuel_end');

            $table->integer('trip_id')->unsigned();
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legs');
    }
}
