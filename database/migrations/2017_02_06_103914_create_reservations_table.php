<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('ref_no')->nullable();
            $table->boolean('completed')->default(false);

            $table->enum('status', ['PENDING', 'ASSIGNED', 'CANCELLED'])->default('PENDING');
            // pending - meaning has yet to be assigned to trips
            // ASSIGNED - meaning has been assigned to trips
            // cancelled - meaning that the trip won't be completed at all

            $table->date('date');
            $table->string('department');
            $table->string('purpose')->nullable();
            $table->string('charging_loc')->nullable();

            // Copied to trips
            $table->date('trip_date');
            $table->string('location');
            $table->string('destination');
            $table->string('pickup_place');
            $table->time('pickup_time');
            $table->string('passenger_contact');

            $table->string('vehicle');
            $table->dateTime('trip_end');
            $table->text('notes')->nullable();

            $table->integer('trip_type_id')->unsigned();
            $table->foreign('trip_type_id')->references('id')->on('trip_types');

            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers');

            $table->integer('requestor_id')->unsigned();
            $table->foreign('requestor_id')->references('id')->on('people');

            $table->integer('authorizer_id')->unsigned();
            $table->foreign('authorizer_id')->references('id')->on('people');

            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('people');

            $table->string('po_document_path')->nullable();

            $table->string('updated_by')->default('System');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
