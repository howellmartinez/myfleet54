<?php

return [

    'per_page' => 25,

    'phones' => ['Mobile', 'Work', 'Home', 'Main', 'Work Fax', 'Home Fax', 'Pager', 'Other'],

    'emails' => ['Home', 'Work', 'Other'],

    'dates' => ['Birthday', 'Anniversary', 'Other'],

    'relationships' => [
        'Assistant', 'Brother', 'Child', 'Domestic Partner', 'Father', 'Friend', 'Manager', 'Mother', 'Parent',
        'Partner', 'Referred By', 'Relative', 'Sister', 'Spouse'
    ],

    'cars' => [
        [
            'type' => 'Sedan',
            'classes' => ['Micro', 'Economy', 'Compact', 'Mid Size', 'Luxury'],
        ],
        [
            'type' => 'Van',
            'classes' => ['Mini MPV', 'MPV', 'Cargo Van', 'Passenger Van', 'Luxury Passenger Van'],
        ],
        [
            'type' => 'SUV',
            'classes' => ['Mini', 'Compact', 'Mid Size', 'Full Size'],
        ]
    ],

    'trip_types' => ['PUDO', '3H', '5H', '8H', '10H'],

    'trip_locations' => ['Metro Manila', 'Out of Town'],
];