/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 211);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Index__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Index___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Index__);


new Vue({
    el: '#index',
    components: { Index: __WEBPACK_IMPORTED_MODULE_0__components_Index___default.a },
    data: {
        filterableFields: filterableFields,
        baseUrl: baseUrl,
        items: [],
        isLoading: true
    }
});

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = {
    props: ['conditions', 'filterable-fields'],

    mounted: function mounted() {
        console.log('Filters components mounted.');
    },


    methods: {
        add: function add() {
            this.$emit('add');
        },
        clear: function clear() {
            this.$emit('clear');
        },
        filter: function filter() {
            this.$emit('filter');
        }
    }
};

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Filters__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Filters___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Filters__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__PaginationLinks__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__PaginationLinks___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__PaginationLinks__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = {

    components: { Filters: __WEBPACK_IMPORTED_MODULE_0__Filters___default.a, PaginationLinks: __WEBPACK_IMPORTED_MODULE_1__PaginationLinks___default.a },

    props: ['filterable-fields', 'base-url'],

    data: function data() {
        return {
            isLoading: false,
            conditions: [],
            items: [],
            pagination: {},
            page: 1
        };
    },


    watch: {
        isLoading: function isLoading(val) {
            console.log('Emitting is loading...', val);
            this.$emit('update-loading', val);
        },
        items: function items(val) {
            console.log('Updating items...');
            this.$emit('update-items', val);
        }
    },

    methods: {
        addCondition: function addCondition() {
            this.conditions.push({
                field: '',
                value: ''
            });
        },
        filter: function filter() {
            this.load(this.assembledUrl);
        },
        clear: function clear() {
            this.conditions = [];
            this.page = 1;
            this.load(this.assembledUrl);
        },
        load: function load(url) {
            this.isLoading = true;
            axios.get(url).then(this.onSuccess).catch(this.onError);
        },
        onSuccess: function onSuccess(response) {
            // console.log(response.data);
            this.pagination = response.data;
            this.items = response.data.data;
            if (this.noItems) this.page = 1;
            this.isLoading = false;
        },
        onError: function onError(error) {
            console.log(error);
            this.isLoading = false;
        },
        prevPage: function prevPage() {
            if (this.isFirstPage || this.isLoading) return;
            this.page--;
            this.load(this.assembledUrl);
        },
        nextPage: function nextPage() {
            if (this.isLastPage || this.isLoading) return;
            this.page++;
            this.load(this.assembledUrl);
        }
    },

    mounted: function mounted() {
        console.log('Index component mounted.');
        this.load(this.baseUrl);
    },


    computed: {
        conditionString: function conditionString() {
            return this.conditions.reduce(function (prev, curr) {
                return prev.concat('&' + curr.field + '=' + curr.value);
            }, '');
        },
        assembledUrl: function assembledUrl() {
            return this.baseUrl + '?page=' + this.page + this.conditionString;
        },
        noItems: function noItems() {
            return this.items.length == 0;
        },
        isLastPage: function isLastPage() {
            if (this.noItems) return true;
            return this.page == this.pagination.last_page;
        },
        isFirstPage: function isFirstPage() {
            return this.page == 1;
        },
        paginationStatus: function paginationStatus() {
            if (this.isLoading) return '';
            return 'Displaying items ' + (this.pagination.from || 0) + ' to ' + (this.pagination.to || 0) + ' of ' + this.pagination.total;
        }
    }
};

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = {
    props: ['status', 'prev-disabled', 'next-disabled'],

    mounted: function mounted() {
        console.log('Pagination Links components mounted.');
    },


    methods: {
        prevPage: function prevPage() {
            this.$emit('prev-page');
        },
        nextPage: function nextPage() {
            this.$emit('next-page');
        }
    }
};

/***/ }),

/***/ 194:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(186),
  /* template */
  __webpack_require__(200),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/hmartinez/Code/laravel/myfleet54/resources/assets/js/components/Filters.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Filters.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a6a76bc", Component.options)
  } else {
    hotAPI.reload("data-v-7a6a76bc", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 195:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(187),
  /* template */
  __webpack_require__(201),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/hmartinez/Code/laravel/myfleet54/resources/assets/js/components/Index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b7cac48e", Component.options)
  } else {
    hotAPI.reload("data-v-b7cac48e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 196:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(188),
  /* template */
  __webpack_require__(199),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/hmartinez/Code/laravel/myfleet54/resources/assets/js/components/PaginationLinks.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] PaginationLinks.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-333d9034", Component.options)
  } else {
    hotAPI.reload("data-v-333d9034", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 199:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-6"
  }, [_c('div', {
    staticClass: "text-muted",
    staticStyle: {
      "vertical-align": "middle"
    }
  }, [_vm._v(_vm._s(_vm.status))])]), _vm._v(" "), _c('div', {
    staticClass: "col-6 text-right"
  }, [_c('button', {
    staticClass: "btn btn-outline-primary",
    attrs: {
      "disabled": _vm.prevDisabled
    },
    on: {
      "click": _vm.prevPage
    }
  }, [_c('i', {
    staticClass: "fa fa-caret-left"
  }), _vm._v(" Prev\n        ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-outline-primary",
    attrs: {
      "disabled": _vm.nextDisabled
    },
    on: {
      "click": _vm.nextPage
    }
  }, [_vm._v("\n            Next "), _c('i', {
    staticClass: "fa fa-caret-right"
  })])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-333d9034", module.exports)
  }
}

/***/ }),

/***/ 200:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('form', {
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.filter($event)
      }
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-8"
  }, _vm._l((_vm.conditions), function(condition, index) {
    return _c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "form-group col-6"
    }, [_c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (condition.field),
        expression: "condition.field"
      }],
      staticClass: "form-control",
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          condition.field = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
        }
      }
    }, _vm._l((_vm.filterableFields), function(field) {
      return _c('option', {
        domProps: {
          "value": field.name
        }
      }, [_vm._v(_vm._s(field.label))])
    }))]), _vm._v(" "), _c('div', {
      staticClass: "form-group col-6"
    }, [_c('div', {
      staticClass: "input-group"
    }, [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (condition.value),
        expression: "condition.value"
      }],
      staticClass: "form-control",
      attrs: {
        "type": "text"
      },
      domProps: {
        "value": (condition.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          condition.value = $event.target.value
        }
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "input-group-btn"
    }, [_c('button', {
      staticClass: "btn btn-secondary",
      attrs: {
        "type": "button"
      },
      on: {
        "click": function($event) {
          _vm.conditions.splice(index, 1)
        }
      }
    }, [_vm._v("×")])])])])])
  })), _vm._v(" "), _c('div', {
    staticClass: "col-4 text-right"
  }, [_c('button', {
    staticClass: "btn btn-link",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.add
    }
  }, [_c('i', {
    staticClass: "fa fa-plus-circle"
  })]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-outline-primary",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": _vm.filter
    }
  }, [_c('i', {
    staticClass: "fa fa-filter"
  }), _vm._v(" Filter\n            ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-secondary",
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.clear
    }
  }, [_c('i', {
    staticClass: "fa fa-ban"
  }), _vm._v(" Clear\n            ")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7a6a76bc", module.exports)
  }
}

/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('filters', {
    attrs: {
      "conditions": _vm.conditions,
      "filterable-fields": _vm.filterableFields
    },
    on: {
      "add": _vm.addCondition,
      "filter": _vm.filter,
      "clear": _vm.clear
    }
  }), _vm._v(" "), _c('br'), _vm._v(" "), _c('h1', [_vm._t("header")], 2), _vm._v(" "), _c('br'), _vm._v(" "), _vm._t("default"), _vm._v(" "), (_vm.isLoading) ? _c('p', {
    staticClass: "text-center"
  }, [_c('i', {
    staticClass: "fa fa-circle-o-notch fa-spin fa-fw"
  }), _vm._v(" Loading...\n    ")]) : _vm._e(), _vm._v(" "), (!_vm.isLoading && _vm.noItems) ? _c('p', {
    staticClass: "text-center"
  }, [_vm._v("\n        No items.\n    ")]) : _vm._e(), _vm._v(" "), _c('hr'), _vm._v(" "), _c('pagination-links', {
    attrs: {
      "status": _vm.paginationStatus,
      "prev-disabled": _vm.isFirstPage || _vm.isLoading,
      "next-disabled": _vm.isLastPage || _vm.isLoading
    },
    on: {
      "next-page": _vm.nextPage,
      "prev-page": _vm.prevPage
    }
  })], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-b7cac48e", module.exports)
  }
}

/***/ }),

/***/ 211:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(155);


/***/ })

/******/ });