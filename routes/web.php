<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'app'], function () {

    Route::get('/', function () {
        return view('home');
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'users'], function () {
            Route::get('permissions', 'UsersPermissionsController@index');
        });
    });

    Route::group(['prefix' => 'api'], function () {
        Route::post('users/permissions', 'UsersPermissionsApiController@store');

        Route::get('receipts', 'ReceiptApiController@index');
        Route::get('payments', 'PaymentApiController@index');
        Route::get('transmittals', 'TransmittalApiController@index');

        Route::get('statements', 'StatementApiController@index');
        Route::get('statements/{statement}/check', 'StatementApiController@check');

        Route::get('bills', 'BillApiController@index');
        Route::get('reservations', 'ReservationApiController@index');
        Route::get('trips', 'TripApiController@index');

        Route::group(['prefix' => 'trip-types'], function () {
            Route::get('/', 'TripTypeApiController@index');
            Route::post('/', 'TripTypeApiController@store');
            Route::patch('{tripType}', 'TripTypeApiController@update');
            Route::delete('{tripType}', 'TripTypeApiController@destroy');
        });

        Route::group(['prefix' => 'customer-groups'], function () {
            Route::get('/', 'CustomerGroupApiController@index');
            Route::post('/', 'CustomerGroupApiController@store');
            Route::patch('{customerGroup}', 'CustomerGroupApiController@update');
            Route::delete('{customerGroup}', 'CustomerGroupApiController@destroy');
        });

        Route::get('cars', 'CarApiController@index');
        Route::get('persons', 'PersonApiController@index');
        Route::get('customers', 'CustomerApiController@index');
        Route::get('drivers', 'DriverApiController@index');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@index');
        Route::get('create', 'UserController@create');
    });

    Route::group(['prefix' => 'customer-groups'], function () {
        Route::get('/', 'CustomerGroupController@index');
        Route::get('create', 'CustomerGroupController@create');
        Route::get('{customerGroup}/edit', 'CustomerGroupController@edit');
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/', 'CustomerController@index');
        Route::get('create', 'CustomerController@create');
        Route::get('{customer}/edit', 'CustomerController@edit');

        Route::post('/', 'CustomerController@store');
        Route::patch('{customer}', 'CustomerController@update');
        Route::delete('{customer}', 'CustomerController@destroy');
    });

    Route::group(['prefix' => 'trip-types'], function () {
        Route::get('/', 'TripTypeController@index');
        Route::get('create', 'TripTypeController@create');
        Route::get('{tripType}/edit', 'TripTypeController@edit');
    });

    Route::group(['prefix' => 'suppliers'], function () {
        Route::get('/', 'SupplierController@index');
        Route::get('create', 'SupplierController@create');
        Route::get('{supplier}/edit', 'SupplierController@edit');

        Route::post('/', 'SupplierController@store');
        Route::patch('{supplier}', 'SupplierController@update');
        Route::delete('{supplier}', 'SupplierController@destroy');
    });

    Route::group(['prefix' => 'reservations'], function () {
        Route::get('/', 'ReservationController@index');
        Route::get('create', 'ReservationController@create');
        Route::get('{reservation}/edit', 'ReservationController@edit');

        Route::post('/', 'ReservationController@store');
        Route::patch('{reservation}', 'ReservationController@update');
        Route::delete('{reservation}', 'ReservationController@destroy');

        Route::group(['prefix' => '{reservation}/dispatches'], function () {
            Route::get('/', 'ReservationDispatchController@index');
        });
    });

    Route::group(['prefix' => 'dispatches'], function () {
        Route::get('/', 'DispatchController@index');
        Route::get('create', 'DispatchController@create');
        Route::get('{dispatch}/edit', 'DispatchController@edit');

        Route::post('/', 'DispatchController@store');
        Route::patch('{dispatch}', 'DispatchController@update');
        Route::delete('{dispatch}', 'DispatchController@destroy');
    });

    Route::group(['prefix' => 'cars'], function () {
        Route::get('/', 'CarController@index');
        Route::get('create', 'CarController@create');
        Route::get('{car}/edit', 'CarController@edit');

        Route::post('/', 'CarController@store');
        Route::patch('{car}', 'CarController@update');
        Route::delete('{car}', 'CarController@destroy');
    });

    Route::group(['prefix' => 'trips'], function () {
        Route::get('/', 'TripController@index');
        Route::get('create', 'TripController@create');
        Route::get('{trip}/edit', 'TripController@edit');

        Route::post('/', 'TripController@store');
        Route::patch('{trip}', 'TripController@update');
        Route::delete('{trip}', 'TripController@destroy');
    });

    Route::group(['prefix' => 'bills'], function () {
        Route::get('/', 'BillController@index');
        Route::get('create', 'BillController@create');
        Route::get('{bill}/edit', 'BillController@edit');

        Route::post('/', 'BillController@store');
        Route::patch('{bill}', 'BillController@update');
        Route::delete('{bill}', 'BillController@destroy');
    });

    Route::group(['prefix' => 'statements'], function () {
        Route::get('/', 'StatementController@index');
        Route::get('create', 'StatementController@create');
        Route::get('{statement}/edit', 'StatementController@edit');

        Route::post('/', 'StatementController@store');
        Route::patch('{statement}', 'StatementController@update');
        Route::delete('{statement}', 'StatementController@destroy');
    });

    Route::group(['prefix' => 'transmittals'], function () {
        Route::get('/', 'TransmittalController@index');
        Route::get('create', 'TransmittalController@create');
        Route::get('{transmittal}/edit', 'TransmittalController@edit');

        Route::post('/', 'TransmittalController@store');
        Route::patch('{transmittal}', 'TransmittalController@update');
        Route::delete('{transmittal}', 'TransmittalController@destroy');
    });

    Route::group(['prefix' => 'payments'], function () {
        Route::get('/', 'PaymentController@index');
        Route::get('create', 'PaymentController@create');
        Route::get('{payment}/edit', 'PaymentController@edit');

        Route::post('/', 'PaymentController@store');
        Route::patch('{payment}', 'PaymentController@update');
        Route::delete('{payment}', 'PaymentController@destroy');
    });

    Route::group(['prefix' => 'receipts'], function () {
        Route::get('/', 'ReceiptController@index');
        Route::get('create', 'ReceiptController@create');
        Route::get('{receipt}/edit', 'ReceiptController@edit');

        Route::post('/', 'ReceiptController@store');
        Route::patch('{receipt}', 'ReceiptController@update');
        Route::delete('{receipt}', 'ReceiptController@destroy');
    });

    Route::group(['prefix' => 'persons'], function () {
        Route::get('/', 'PersonController@index');
        Route::get('create', 'PersonController@create');
        Route::get('{person}/edit', 'PersonController@edit');

        Route::post('/', 'PersonController@store');
        Route::patch('{person}', 'PersonController@update');
        Route::delete('{person}', 'PersonController@destroy');
    });

    Route::group(['prefix' => 'drivers'], function () {
        Route::get('/', 'DriverController@index');
        Route::get('create', 'DriverController@create');
        Route::get('{driver}/edit', 'DriverController@edit');

        Route::post('/', 'DriverController@store');
        Route::patch('{driver}', 'DriverController@update');
        Route::delete('{driver}', 'DriverController@destroy');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index');
