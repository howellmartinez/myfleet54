<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form2307 extends Model
{
    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }
}
