<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Receipt extends SerializableFilterableModel
{
    public static $prefix = 'REC';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    public $fillable = ['date', 'total', 'customer_id', 'ref_no'];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('customer', function($customer) use ($value) {
            return $customer->where('name', 'LIKE', $value);
        });
    }

    public function getEditUrlAttribute()
    {
        return action('ReceiptController@edit', $this->id);
    }
}
