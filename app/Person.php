<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Person extends SerializableFilterableModel
{
    public static $prefix = 'PER';

    public static $filterableFields = [
        ['label' => 'First Name', 'name' => 'first_name']
    ];

    protected $appends = ['full_name', 'serial', 'edit_url'];

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'nickname',
        'phones',
        'emails',
        'relationships',
        'others',
        'notes',
    ];

    public function customerPersons()
    {
        return $this->hasMany(CustomerPerson::class);
    }

    public function drivers()
    {
        return $this->hasMany(Driver::class);
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class)->withPivot(['department', 'position']);
    }

    /*
     * Custom Methods
     */

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getEditUrlAttribute()
    {
        return action('PersonController@edit', $this->id);
    }


}
