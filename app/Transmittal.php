<?php

namespace App;


use App\myfleet\SerializableFilterableModel;

class Transmittal extends SerializableFilterableModel
{
    public static $prefix = 'TRA';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    protected $fillable = ['date', 'customer_id', 'ref_no'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function statements()
    {
        return $this->hasMany(Statement::class);
    }

    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('customer', function($customer) use ($value) {
            return $customer->where('name', 'LIKE', $value);
        });
    }

    public function getEditUrlAttribute()
    {
        return action('TransmittalController@edit', $this->id);
    }
}
