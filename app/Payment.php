<?php

namespace App;


use App\myfleet\SerializableFilterableModel;

class Payment extends SerializableFilterableModel
{
    public static $prefix = 'PAY';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    public $fillable = ['date', 'total', 'customer_id', 'statement_id', 'receipt_id', 'ref_no'];

    public function statement()
    {
        return $this->belongsTo(Statement::class);
    }

    public function receipt()
    {
        return $this->belongsTo(Receipt::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /*
     * Query Scopes
     */
    public function scopeUnreceipted($query)
    {
        return $query->doesntHave('receipt');
    }

    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('customer', function($customer) use ($value) {
            return $customer->where('name', 'LIKE', $value);
        });
    }

    public function getEditUrlAttribute()
    {
        return action('PaymentController@edit', $this->id);
    }
}
