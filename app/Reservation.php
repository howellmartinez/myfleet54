<?php

namespace App;

use App\myfleet\SerializableFilterableModel;
use Carbon\Carbon;

class Reservation extends SerializableFilterableModel
{
    public static $prefix = 'RES';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    public function getEditUrlAttribute()
    {
        return action('ReservationController@edit', $this->id);
    }

    public $dates = ['trip_end'];

    public function setTripEndAttribute($value)
    {
        $this->attributes['trip_end'] = Carbon::parse($value);
    }

    protected $fillable = [
//        'completed',
        'date',
        'department',
        'purpose',
        'charging_loc',
        'trip_date',
        'location',
        'vehicle',
        'pickup_place',
        'pickup_time',
        'destination',
        'passenger_contact',
        'trip_end',
        'notes',
        'trip_type_id',
        'customer_id',
        'requestor_id',
        'authorizer_id',
        'passenger_id',
        'ref_no',
        'po_document_path',
    ];

    public function tripType()
    {
        return $this->belongsTo(TripType::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function authorizer()
    {
        return $this->belongsTo(Person::class, 'authorizer_id');
    }

    public function passenger()
    {
        return $this->belongsTo(Person::class, 'passenger_id');
    }

    public function requestor()
    {
        return $this->belongsTo(Person::class, 'requestor_id');
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('customer', function ($customer) use ($value) {
            return $customer->where('name', 'LIKE', '%'.$value.'%');
        });
    }


}
