<?php

namespace App\Http\Requests;

use App\Reservation;
use App\Trip;
use Illuminate\Foundation\Http\FormRequest;

class DispatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(Reservation $reservation, Trip $trip = null)
    {
        if ($trip != null) {

        } else {

        }
    }
}
