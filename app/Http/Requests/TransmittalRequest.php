<?php

namespace App\Http\Requests;
use App\Statement;
use App\Transmittal;
use Illuminate\Foundation\Http\FormRequest;

class TransmittalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(Transmittal $transmittal = null)
    {
        if ($transmittal != null) {
            $transmittal->update($this->all());
        } else {
            $transmittal = Transmittal::create($this->all());
        }

        $statement_ids = json_decode($this->input('statements', []));

        Statement::whereIn('id', $statement_ids)->update(['transmittal_id' => $transmittal->id]);

        // Untag statements where transmittal id is still transmittal id but not in the array
        Statement::where('transmittal_id', $transmittal->id)
            ->whereNotIn('id', $statement_ids)
            ->update(['transmittal_id' => null]);
    }
}
