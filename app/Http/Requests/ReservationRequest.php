<?php

namespace App\Http\Requests;

use App\Reservation;
use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'department' => 'required',
            'purpose' => 'required',
            'charging_loc' => 'required',
            'trip_date' => 'required',
            'type' => 'required',
            'location' => 'required',
            'vehicle' => 'required',
            'pickup_place' => 'required',
            'pickup_time' => 'required',
            'destination' => 'required',
            'passenger_contact' => 'required',
            'trip_end' => 'required',
            'customer_id' => 'required',
            'requestor_id' => 'required',
            'authorizer_id' => 'required',
            'passenger_id' => 'required',
            'ref_no' => 'required',
//            'po_document_path' => 'required',
//            'completed' => 'required',
        ];
    }

    public function persist(Reservation $reservation = null)
    {
        \DB::transaction(function() use ($reservation) {
            if ($reservation != null) {
                $reservation->update($this->all());
            } else {
                $reservation = Reservation::create($this->all());
            }
        });
        return $reservation;
    }
}
