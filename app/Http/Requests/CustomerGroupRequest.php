<?php

namespace App\Http\Requests;

use App\CustomerGroup;
use Illuminate\Foundation\Http\FormRequest;

class CustomerGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(CustomerGroup $customerGroup = null)
    {
        if ($customerGroup != null) {
            $customerGroup->update($this->all());
        } else {
            $customerGroup = CustomerGroup::create($this->all());
        }
    }
}
