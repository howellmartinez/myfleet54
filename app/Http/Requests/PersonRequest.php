<?php

namespace App\Http\Requests;

use App\Customer;
use App\Driver;
use App\Person;
use Illuminate\Foundation\Http\FormRequest;

class PersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required',
            'first_name' => 'required',
        ];
    }

    public function persist(Person $person = null)
    {
        \DB::transaction(function () use ($person) {
            if ($person != null) {
                $person->update($this->all());
            } else {
                $person = Person::create($this->all());
            }

            // If this person is set as primary,
            // set all the other people in same company as non-primary
//            if ( $person->primary ) {
//                Person::whereCustomerId($person->customer_id)
//                    ->primary()->where('id', '<>', $person->id)
//                    ->update(['primary' => false]);
//            }

            $drivers = array_map(function ($driver) {
                return Driver::firstOrNew(['id' => $driver->id ?? ''])
                    ->fill([
                        'notes' => $driver->notes
                    ]);
            }, json_decode($this->input('drivers', [])));

            $person->drivers()->saveMany($drivers);

            // Delete drivers where person_id is still the person's id but not in the array
            Driver::where('person_id', $person->id)
                ->whereNotIn('id', array_pluck($drivers, ['id']))->delete();

            $customerPeople = [];

            foreach (json_decode($this->input('customers', [])) as $customerPerson) {
                $customerPeople[$customerPerson->pivot->customer_id] = [
                    'position' => $customerPerson->pivot->position,
                    'department' => $customerPerson->pivot->department,
                ];
            }

            $person->customers()->sync($customerPeople);


            //        Statement::whereIn('id', $statement_ids)->update(['transmittal_id' => $transmittal->id]);

            // Untag statements where transmittal id is still transmittal id but not in the array
//        Statement::where('transmittal_id', $transmittal->id)
//            ->whereNotIn('id', $statement_ids)
//            ->update(['transmittal_id' => null]);

            // Extract drivers


        });

        return $person;
    }
}
