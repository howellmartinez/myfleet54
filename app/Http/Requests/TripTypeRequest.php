<?php

namespace App\Http\Requests;

use App\TripType;
use Illuminate\Foundation\Http\FormRequest;

class TripTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(TripType $tripType = null)
    {
        if ($tripType != null) {
            $tripType->update($this->all());
        } else {
            $tripType = TripType::create($this->all());
        }
    }
}
