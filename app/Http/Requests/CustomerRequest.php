<?php

namespace App\Http\Requests;

use App\Customer;
use App\Person;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $customer_id = $this->route('customer')->id ?? null;
        return [
            'name' => 'required|unique:customers,name,' . $customer_id,
            'code' => 'required|unique:customers,code,' . $customer_id,
        ];
    }

    public function persist(Customer $customer = null)
    {
        \DB::transaction(function() use ($customer) {
            if ($customer != null) {
                $customer->update($this->all());
            } else {
                $customer = Customer::create($this->all());
//            $phone_json = json_encode([[
//                'label' => 'Main',
//                'value' => $this->input('phone')
//            ]]);
//            $person = new Person([
//                'first_name' => explode(' ', $this->input('person'))[0],
//                'last_name' => explode(' ', $this->input('person'))[1],
//                'primary' => true,
//                'phones' => $phone_json,
//            ]);
//            $customer->people()->save($person);
            }
        });
        return $customer;

    }
}
