<?php

namespace App\Http\Requests;

use App\Car;
use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class' => 'required',
            'make' => 'required',
            'model' => 'required',
            'fuel' => 'required',
            'tx' => 'required',
            'year' => 'required',
            'color' => 'required',
            'max_cap' => 'required',
            'conduction_no' => 'required',
            'engine_no' => 'required',
            'serial_no' => 'required',
            'or_no' => 'required',
            'cr_no' => 'required',
        ];
    }

    public function persist(Car $car = null)
    {
        \DB::transaction(function() use ($car) {
            $attributes = $this->all();
            if ($car != null) {
                $car->update($attributes);
            } else {
                Car::create($attributes);
            }
        });

        return $car;

    }
}
