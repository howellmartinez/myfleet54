<?php

namespace App\Http\Requests;

use App\Bill;
use Illuminate\Foundation\Http\FormRequest;

class BillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip_id' => 'required',
            'approved_by' => 'required',
        ];
    }

    public function persist(Bill $bill = null)
    {
        if ($bill != null) {
            $bill->update($this->all());
        } else {
            Bill::create($this->all());
        }
    }
}
