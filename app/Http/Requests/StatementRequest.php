<?php

namespace App\Http\Requests;

use App\Bill;
use App\Statement;
use Illuminate\Foundation\Http\FormRequest;

class StatementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(Statement $statement = null)
    {
        if ($statement != null) {
            $statement->update($this->all());
        } else {
            $statement = Statement::create($this->all());
        }

        $bill_ids = json_decode($this->input('bills', []));

        Bill::whereIn('id', $bill_ids)->update(['statement_id' => $statement->id]);

        // Untag bills where statement id is still statement id but not in the array
        Bill::where('statement_id', $statement->id)->whereNotIn('id', $bill_ids)->update(['statement_id' => null]);
    }
}
