<?php

namespace App\Http\Requests;

use App\Leg;
use App\Reservation;
use App\Trip;
use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_no' => 'required',
            'date' => 'required',
            'type' => 'required',
            'location' => 'required',
            'destination' => 'required',
            'pickup_place' => 'required',
            'pickup_time' => 'required',
            'passenger_id' => 'required',
            'passenger_contact' => 'required',

            'driver_id' => 'required',
            'car_id' => 'required',
            'reservation_id' => 'required',
        ];
    }

    public function persist(Trip $trip = null)
    {
        \DB::transaction(function() use ($trip) {
            if ($trip != null) {
                $trip->fill($this->all());
            } else {
                $trip = new Trip($this->all());
            }

            $legs = array_map(function($leg) {
                return Leg::firstOrNew([
                    'id' => array_get($leg, 'id', null),
                ])->fill($leg);
            }, json_decode($this->input('legs', []), true));

            $reservation = Reservation::find($this->input('reservation_id'));
            $reservation->trips()->save($trip)->legs()->saveMany($legs);

            // Delete removed legs
            Leg::whereTripId($trip->id)
                ->whereNotIn('id', array_pluck($legs, ['id']))
                ->delete();
        });
    }
}
