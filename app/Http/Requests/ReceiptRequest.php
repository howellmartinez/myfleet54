<?php

namespace App\Http\Requests;

use App\Payment;
use App\Receipt;
use Illuminate\Foundation\Http\FormRequest;

class ReceiptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(Receipt $receipt = null)
    {
        if ($receipt != null) {
            $receipt->update($this->all());
        } else {
            $receipt = Receipt::create($this->all());
        }

        $payment_ids = json_decode($this->input('payments', []));

        Payment::whereIn('id', $payment_ids)->update(['receipt_id' => $receipt->id]);

        // Untag payments where receipt id is still receipt id but not in the array
        Payment::where('receipt_id', $receipt->id)
            ->whereNotIn('id', $payment_ids)
            ->update(['receipt_id' => null]);
    }
}
