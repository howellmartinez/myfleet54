<?php

namespace App\Http\Requests;

use App\Driver;
use Illuminate\Foundation\Http\FormRequest;

class DriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required',
            'first_name' => 'required',
        ];
    }

    public function persist(Driver $driver = null)
    {
        \DB::transaction(function() use ($driver) {
            if ($driver!=null) {
                $driver->update($this->all());
            } else {
                Driver::create($this->all());
            }
        });
        return $driver;
    }
}
