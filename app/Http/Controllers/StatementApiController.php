<?php

namespace App\Http\Controllers;

use App\Statement;
use Illuminate\Http\Request;

class StatementApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $statements = Statement::with('customer')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $statements;
    }

    public function check(Statement $statement)
    {
        // Set to null
        $bills = $statement->bills()->count();
        $transmittals = $statement->transmittal()->count();

        return response()->json([
            'bills' => $bills,
            'transmittals' => $transmittals,
        ]);
    }
}
