<?php

namespace App\Http\Controllers;

use App\CustomerGroup;
use App\Http\Requests\CustomerGroupRequest;
use Illuminate\Http\Request;

class CustomerGroupApiController extends Controller
{
    public function index()
    {
        $customerGroups = CustomerGroup::orderBy('name')->paginate();
        return $customerGroups;
    }

    public function store(CustomerGroupRequest $request)
    {
        $request->persist();
    }

    public function update(CustomerGroupRequest $request, CustomerGroup $customerGroup)
    {
        $request->persist($customerGroup);
    }

    public function delete()
    {

    }
}
