<?php

namespace App\Http\Controllers;

use App\Http\Requests\TripTypeRequest;
use App\TripType;
use Illuminate\Http\Request;

class TripTypeApiController extends Controller
{
    public function index()
    {
        $trips = TripType::orderBy('name')->paginate();
        return $trips;
    }

    public function store(TripTypeRequest $request)
    {
        $request->persist();
    }

    public function update(TripTypeRequest $request, TripType $tripType)
    {
        $request->persist($tripType);
    }

    public function delete(TripType $tripType)
    {

    }

}
