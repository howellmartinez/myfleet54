<?php

namespace App\Http\Controllers;

use App\TripType;
use Illuminate\Http\Request;

class TripTypeController extends Controller
{
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => [],
            'baseUrl' => action('TripTypeApiController@index'),
        ]);
        return view('trip_types.index');
    }

    public function create()
    {
        \JavaScript::put(['tripType' => null]);
        return view('trip_types.create');
    }

    public function edit(TripType $tripType)
    {
        \JavaScript::put(['tripType' => $tripType]);
        return view('trip_types.edit');
    }

}
