<?php

namespace App\Http\Controllers;

use App\Receipt;
use Illuminate\Http\Request;

class ReceiptApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $receipts = Receipt::with('customer')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $receipts;
    }
}
