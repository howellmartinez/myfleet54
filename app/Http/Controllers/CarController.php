<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\CarRequest;
use App\Repositories\Cars;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Car::$filterableFields,
            'baseUrl' => action('CarApiController@index'),
        ]);
        return view('cars.index');
    }

    public function create()
    {
        $car = null;
        $classifications = config('myfleet.cars');
        \JavaScript::put(compact('car', 'classifications'));
        return view('cars.create');
    }

    public function edit(Car $car)
    {
        $classifications = config('myfleet.cars');
        \JavaScript::put(compact('car', 'classifications'));
        return view('cars.edit');
    }

    public function store(CarRequest $request)
    {
        $request->persist();
//        flash()->success('Car Created');
//        return redirect()->action('CarController@index');
    }

    public function update(CarRequest $request, Car $car)
    {
        return $request->persist($car);
//        flash()->success('Car Updated');
//        return redirect()->action('CarController@index');
    }

    public function destroy(Car $car)
    {
        $car->delete();
//        flash()->success('Car Deleted');
//        return redirect()->action('CarController@index');
    }


}
