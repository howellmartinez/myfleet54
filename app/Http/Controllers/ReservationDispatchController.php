<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;

class ReservationDispatchController extends Controller
{
    public function index(Reservation $reservation)
    {
        return view('reservations.dispatches.index');
    }
}
