<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\TransmittalRequest;
use App\Statement;
use App\Transmittal;
use Illuminate\Http\Request;

class TransmittalController extends Controller
{
    use MyControllerTrait;

    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Transmittal::$filterableFields,
            'baseUrl' => action('TransmittalApiController@index'),
        ]);
        return view('transmittals.index');
    }

    public function create()
    {
        $customers = Customer::with('statements')->get();
        $transmittal = null;
        $statements = Statement::doesntHave('transmittal')->get();
        \JavaScript::put(compact('customers', 'statements', 'transmittal'));
        return view('transmittals.create');
    }

    public function edit($id)
    {
        $transmittal = Transmittal::with('statements')->get();
        $customers = Customer::with('statements')->get();
        $statements = Statement::doesntHave('transmittal')
            ->orWhere('transmittal_id', $id)
            ->get();
        \JavaScript::put(compact('customers', 'statements', 'transmittal'));
        return view('transmittals.edit');
    }

    public function store(TransmittalRequest $request)
    {
        $request->persist();
        return $this->redirectToAction($request, 'TransmittalController@index');
    }

    public function update(TransmittalRequest $request, Transmittal $transmittal)
    {
        $request->persist($transmittal);
        return $this->redirectToAction($request, 'TransmittalController@index');
    }

    public function destroy(Transmittal $transmittal)
    {
        $transmittal->delete();
        return $this->redirectToAction(request(), 'TransmittalController@index');
    }
}
