<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;

class CarApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $cars = Car::filter($filters)
            ->orderBy('id')
            ->paginate();
        return $cars;
    }
}
