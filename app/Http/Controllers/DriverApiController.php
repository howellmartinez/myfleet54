<?php

namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;

class DriverApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $drivers = Driver::with('person')->filter($filters)
//            ->orderBy('last_name')
//            ->orderBy('first_name')
            ->paginate();
        return $drivers;
    }
}
