<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\PaymentRequest;
use App\Payment;
use App\Statement;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    use MyControllerTrait;

    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Payment::$filterableFields,
            'baseUrl' => action('PaymentApiController@index'),
        ]);
        return view('payments.index');
    }

    public function create()
    {
        $customers = Customer::all();
        $statements = Statement::all();
        $payment = null;
        \JavaScript::put(compact('customers', 'payment', 'statements'));
        return view('payments.create');
    }

    public function edit(Payment $payment)
    {
        $customers = Customer::all();
        $statements = Statement::all();
        \JavaScript::put(compact('customers', 'payment', 'statements'));
        return view('payments.edit', compact('payment'));
    }

    public function store(PaymentRequest $request)
    {
        $request->persist();
//        return $this->redirectToAction($request, 'PaymentController@index');
    }

    public function update(PaymentRequest $request, Payment $payment)
    {
        $request->persist($payment);
//        return $this->redirectToAction($request, 'PaymentController@index');
    }

    public function destroy(Payment $payment)
    {
        $payment->delete();
//        return $this->redirectToAction(request(), 'PaymentController@index');
    }
}
