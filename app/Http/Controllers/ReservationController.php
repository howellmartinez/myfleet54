<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\ReservationRequest;
use App\Person;
use App\Reservation;
use App\TripType;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Reservation::$filterableFields,
            'baseUrl' => action('ReservationApiController@index'),
        ]);
        return view('reservations.index');}

    public function create()
    {
        $reservation = null;
        $customers = Customer::orderBy('name')->get();
        $persons = Person::orderBy('last_name')->orderBy('first_name')->get();
        $tripTypes = TripType::orderBy('name')->get();
        \JavaScript::put(compact('reservation', 'customers', 'persons', 'tripTypes'));
        return view('reservations.create');
    }

    public function edit(Reservation $reservation)
    {
        $customers = Customer::orderBy('name')->get();
        $persons = Person::orderBy('last_name')->orderBy('first_name')->get();
        \JavaScript::put(compact('reservation', 'customers', 'persons'));
        return view('reservations.edit');
    }

    public function store(ReservationRequest $request)
    {
        $request->persist();
//        flash()->success('Reservation Created.');
//        return redirect()->action('ReservationController@index');
    }

    public function update(ReservationRequest $request, Reservation $reservation)
    {
        return $request->persist($reservation);
//        flash()->success('Reservation Updated.');
//        return redirect()->action('ReservationController@index');
    }

    public function destroy(Reservation $reservation)
    {
        $reservation->delete();
//        flash()->success('Reservation Deleted.');
//        return redirect()->action('ReservationController@index');
    }
}
