<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\PersonRequest;
use App\Person;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Person::$filterableFields,
            'baseUrl' => action('PersonApiController@index'),
        ]);
        return view('persons.index');
    }

    public function create()
    {
        $customers = Customer::orderBy('name')->get();
        $person = null;
        \JavaScript::put(compact('person', 'customers'));
        return view('persons.create');
    }

    public function edit($id)
    {
        $person = Person::with('customers', 'drivers')->find($id);
        $customers = Customer::orderBy('name')->get();
        \JavaScript::put(compact('person', 'customers'));
        return view('persons.edit');
    }

    public function store(PersonRequest $request)
    {
        $request->persist();
    }

    public function update(PersonRequest $request, Person $person)
    {
        return $request->persist($person);
    }

    public function destroy(Request $request, Person $person)
    {
        $person->delete();
    }

}
