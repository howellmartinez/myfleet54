<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerGroup;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Customer::$filterableFields,
            'baseUrl' => action('CustomerApiController@index'),
        ]);
        return view('customers.index');
    }

    public function create()
    {
        $customer = null;
        $customerGroups = CustomerGroup::orderBy('code')->get();
        \JavaScript::put(compact('customer', 'customerGroups'));
        return view('customers.create');
    }

    public function edit(Customer $customer)
    {
        $customerGroups = CustomerGroup::orderBy('code')->get();
        \JavaScript::put(compact('customer', 'customerGroups'));
        return view('customers.edit');
    }

    public function store(CustomerRequest $request)
    {
        $request->persist();
//        if ($request->isJson() || $request->wantsJson()) {
//            return response()->json([
//                'url' =>  action('CustomerController@index', [], false),
//            ]);
//        }
//        flash('Customer Created', 'success');
//        return redirect()->action('CustomerController@index');
    }



    public function update(CustomerRequest $request, Customer $customer)
    {
        return $request->persist($customer);

//        flash('Customer Updated', 'success');
//        return redirect()->action('CustomerController@index');
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();
//        flash('Customer Deleted', 'success');
//        return redirect()->action('CustomerController@index');
    }
}
