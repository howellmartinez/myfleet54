<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersPermissionsRequest;
use Illuminate\Http\Request;

class UsersPermissionsApiController extends Controller
{
    public function store(UsersPermissionsRequest $request)
    {
        $request->persist();
    }
}
