<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

class PersonApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $persons = Person::filter($filters)
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->paginate();

//        $persons = Person::select('id', \DB::raw('concat_ws(" ", first_name, last_name) as name'))
//            ->groupBy('id')
//            ->having('name', 'LIKE', 'gianni%')
//            ->get();

        return $persons;
    }
}
