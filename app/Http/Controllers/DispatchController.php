<?php

namespace App\Http\Controllers;

use App\Car;
use App\Dispatch;
use App\Driver;
use App\Http\Requests\DispatchRequest;
use App\Reservation;
use Illuminate\Http\Request;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;

class DispatchController extends Controller
{
    public function index()
    {
        $dispatches = Dispatch::all();
        return view('dispatches.index', compact('dispatches'));
    }

    public function create()
    {
        $reservations = Reservation::with('customer', 'authorizer', 'requestor')->get();
        $cars = Car::all();
        $drivers = Driver::all();
        JavaScriptFacade::put(compact('reservations'));
        return view('dispatches.create', compact('cars', 'drivers'));
    }

    public function store(DispatchRequest $request, Reservation $reservation)
    {
        $request->persist($reservation);
        return redirect()->action('DispatchController@index', $reservation->id);
    }

    public function update(DispatchRequest $request, Reservation $reservation, Trip $trip)
    {
        $request->persist($reservation, $trip);
        return redirect()->action('DispatchController@index', $reservation->id);
    }
}
