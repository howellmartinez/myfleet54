<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\ReceiptRequest;
use App\Payment;
use App\Receipt;
use Illuminate\Http\Request;

class ReceiptController extends Controller
{
    use MyControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Receipt::$filterableFields,
            'baseUrl' => action('ReceiptApiController@index'),
        ]);
        return view('receipts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $payments = Payment::unreceipted()->get();
        $receipt = null;
        \JavaScript::put(compact('customers', 'payments', 'receipt'));
        return view('receipts.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Receipt $receipt
     * @internal param int $id
     */
    public function edit($id)
    {
        $receipt = Receipt::with('payments')->find($id);
        $customers = Customer::all();
        $payments = Payment::unreceipted()->orWhere('receipt_id', $id)->get();
        \JavaScript::put(compact('customers', 'payments', 'receipt'));
        return view('receipts.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReceiptRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceiptRequest $request)
    {
        $request->persist();
        return $this->redirectToAction($request, 'ReceiptController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReceiptRequest|Request $request
     * @param Receipt $receipt
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(ReceiptRequest $request, Receipt $receipt)
    {
        $request->persist($receipt);
        return $this->redirectToAction($request, 'ReceiptController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Receipt $receipt
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Receipt $receipt)
    {
        $receipt->delete();
        return $this->redirectToAction(request(), 'ReceiptController@index');

    }
}
