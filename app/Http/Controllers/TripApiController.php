<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;

class TripApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $trips = Trip::with('reservation.customer', 'car')
            ->filter($filters)
            ->orderBy('ticket_no', 'desc')
            ->paginate();
        return $trips;


//        $current_page = LengthAwarePaginator::resolveCurrentPage();
//        $per_page = 2;
//        $page_results = $grouped_klasses->slice(($current_page - 1) * $per_page, $per_page);
//        $paginator = new LengthAwarePaginator($page_results, $grouped_klasses->count(), 2, $current_page);
//        $paginator->setPath('classes');
//        return view('faculty.admin.klasses.index', compact('grouped_klasses', 'paginator'));
    }
}
