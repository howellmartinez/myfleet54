<?php

namespace App\Http\Controllers;

use App\Car;
use App\Driver;
use App\Http\Requests\TripRequest;
use App\Person;
use App\Repositories\Trips;
use App\Reservation;
use App\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    use MyControllerTrait;

    private $repository;

    function __construct(Trips $repository) {
        $this->repository = $repository;
    }

    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Trip::$filterableFields,
            'baseUrl' => action('TripApiController@index'),
        ]);
        return view('trips.index');
    }

    public function create()
    {
        // TODO reservatons that are still incomplete
        $reservations = Reservation::with('customer')->get();
        $cars = Car::all();
        $drivers = Driver::all();
        $persons = Person::all();
        $trip = null;

        $trip_types = config('myfleet.trip_types');
        $locations = config('myfleet.trip_locations');

        \JavaScript::put(compact('trip', 'cars', 'drivers', 'reservations', 'trip_types', 'locations', 'persons'));

        return view('trips.create');
    }

    public function edit($id)
    {
        $trip = Trip::with('legs')->find($id);
        $reservations = Reservation::with('customer')->get();
        $cars = Car::all();
        $drivers = Driver::all();
        $persons = Person::all();

        $trip_types = config('myfleet.trip_types');
        $locations = config('myfleet.trip_locations');

        \JavaScript::put(compact('trip', 'cars', 'drivers', 'reservations', 'trip_types', 'locations', 'persons'));

        return view('trips.edit', compact('trip'));
    }

    public function store(TripRequest $request)
    {
        $request->persist();
        return $this->redirectToAction($request, 'TripController@index');
    }

    public function update(TripRequest $request, Trip $trip)
    {
        $request->persist($trip);
        return $this->redirectToAction($request, 'TripController@index');
    }

    public function destroy(Trip $trip)
    {
        $trip->delete();
        return $this->redirectToAction(request(), 'TripController@index');
    }
}
