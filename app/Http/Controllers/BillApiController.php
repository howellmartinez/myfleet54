<?php

namespace App\Http\Controllers;

use App\Bill;
use Illuminate\Http\Request;

class BillApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $bills = Bill::with('trip.reservation.customer')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $bills;
    }
}