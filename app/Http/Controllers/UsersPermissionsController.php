<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class UsersPermissionsController extends Controller
{
    public function index()
    {
        $permissionGroups = Permission::get()->groupBy(function($permission) {
            return explode(' ', $permission->name)[1];
        });

        $users = User::with('permissions')->get();

        \JavaScript::put(compact('permissionGroups', 'users'));

        return view('users.permissions.index');
    }
}
