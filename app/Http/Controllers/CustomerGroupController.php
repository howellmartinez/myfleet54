<?php

namespace App\Http\Controllers;

use App\CustomerGroup;
use Illuminate\Http\Request;

class CustomerGroupController extends Controller
{
    public function index() {
        \JavaScript::put([
            'filterableFields' => [],
            'baseUrl' => action('CustomerGroupApiController@index'),
        ]);
        return view('customer_groups.index');
    }

    public function create()
    {
        \JavaScript::put(['customerGroup' => null]);
        return view('customer_groups.create');
    }

    public function edit(CustomerGroup $customerGroup) {}
}
