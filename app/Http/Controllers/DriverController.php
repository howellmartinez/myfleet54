<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Requests\DriverRequest;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    use MyControllerTrait;

    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Driver::$filterableFields,
            'baseUrl' => action('DriverApiController@index'),
        ]);
        return view('drivers.index');
    }

    public function create()
    {
        return view('drivers.create');
    }

    public function edit(Driver $driver)
    {
        \JavaScript::put(compact('driver'));
        return view('drivers.edit');
    }

    public function store(DriverRequest $request)
    {
        $request->persist();
//        flash()->success('Driver Created');
//        return redirect()->action('DriverController@index');
    }

    public function update(DriverRequest $request, Driver $driver)
    {
        return $request->persist($driver);
//        flash()->success('Driver Updated');
//        return redirect()->action('DriverController@index');
    }

    public function destroy(Driver $driver)
    {
        $driver->delete();
//        flash()->success('Driver Deleted');
//        return redirect()->action('DriverController@index');
    }
}
