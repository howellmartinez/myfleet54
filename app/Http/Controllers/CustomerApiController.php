<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $customers = Customer::with('primaryPerson', 'customerGroup')
            ->filter($filters)
            ->orderBy('name')
            ->paginate();
        return $customers;
    }
}
