<?php

namespace App\Http\Controllers;

use App\Transmittal;
use Illuminate\Http\Request;

class TransmittalApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $transmittals = Transmittal::with('customer')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $transmittals;
    }
}
