<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;

class ReservationApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $reservations = Reservation::with('customer')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $reservations;
    }
}
