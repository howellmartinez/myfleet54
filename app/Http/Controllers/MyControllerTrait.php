<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

trait MyControllerTrait
{
    protected function redirectTo(Request $request, $url)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'url' =>  $url,
            ]);
        }
        // TODO does this work?
        return redirect($url);
    }

    protected function redirectToAction(Request $request, $action)
    {
        return $this->redirectTo($request, action($action));
    }
}