<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO
        // except page
        // except sortBy
        $filters = $request->all();
        $payments = Payment::with('customer', 'statement')
            ->filter($filters)
            ->orderBy('ref_no', 'desc')
            ->paginate();
        return $payments;
    }
}
