<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\StatementRequest;
use App\Statement;
use App\Trip;
use Illuminate\Http\Request;

class StatementController extends Controller
{
    use MyControllerTrait;

    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Statement::$filterableFields,
            'baseUrl' => action('StatementApiController@index'),
        ]);
        return view('statements.index');
    }

    public function create()
    {
        // TODO should not show billed trips that already have a statement

        $trips = Trip::with(['bill' => function($bill) {
            return $bill->doesntHave('statement');
        },'reservation', 'passenger', 'car'])->billed()->get();

        $customers = Customer::all();
//        with(
//            'billedTrips.bill',
//            'billedTrips.reservation',
//            'billedTrips.passenger',
//            'billedTrips.car')->get();

        $statement = null;

        \JavaScript::put(compact('customers', 'statement', 'trips'));

        return view('statements.create');
    }

    public function edit($id)
    {
        $customers = Customer::all();

        // TODO should not show billed trips that already have a statement
//        $customers = Customer::with(
//            'billedTrips.bill',
//            'billedTrips.reservation',
//            'billedTrips.passenger',
//            'billedTrips.car')
//            ->get();

        $statement = Statement::with('bills')->find($id);

        $trips = Trip::with(['bill' => function($bill) use ($id) {
            return $bill->doesntHave('statement')->orWhere('statement_id', $id);
        },'reservation', 'passenger', 'car'])->billed()->get();

        \JavaScript::put(compact('customers', 'statement', 'trips'));
        return view('statements.edit');
    }

    public function store(StatementRequest $request)
    {
        $request->persist();
        return $this->redirectToAction($request, 'StatementController@index');
    }

    public function update(StatementRequest $request, Statement $statement)
    {
        $request->persist($statement);
        return $this->redirectToAction($request, 'StatementController@index');
    }

    public function destroy(Statement $statement)
    {
        $statement->delete();
        // Deleting this statement may affect attached payments. Continue?p
        return $this->redirectToAction(request(), 'StatementController@index');
    }
}
