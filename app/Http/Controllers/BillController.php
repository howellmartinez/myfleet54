<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Http\Requests\BillRequest;
use App\Trip;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BillController extends Controller
{
    use MyControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        \JavaScript::put([
            'filterableFields' => Bill::$filterableFields,
            'baseUrl' => action('BillApiController@index'),
        ]);
        return view('bills.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $trips = Trip::complete()->unbilled()->with('reservation.customer', 'car', 'passenger', 'reservation.requestor', 'reservation.authorizer')->get();
        $bill = null;
        \JavaScript::put(compact('trips', 'bill'));
        return view('bills.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Bill $bill
     * @return Response
     * @internal param int $id
     */
    public function edit(Bill $bill)
    {
        $trips = Trip::complete()->unbilled()->orWhere('id', $bill->trip_id)
            ->with('reservation.customer', 'reservation.requestor', 'reservation.authorizer',
                'car', 'passenger')->get();
        \JavaScript::put(compact('trips', 'bill'));
        return view('bills.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BillRequest|Request $request
     * @return Response
     */
    public function store(BillRequest $request)
    {
        $request->persist();
        return $this->redirectToAction($request, 'BillController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BillRequest|Request $request
     * @param Bill $bill
     * @return Response
     * @internal param int $id
     */
    public function update(BillRequest $request, Bill $bill)
    {
        $request->persist($bill);
        return $this->redirectToAction($request, 'BillController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bill $bill
     * @return Response
     * @internal param int $id
     */
    public function destroy(Bill $bill)
    {
        $bill->delete();
        return $this->redirectToAction(request(), 'BillController@index');
    }
}
