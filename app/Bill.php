<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Bill extends SerializableFilterableModel
{
    public static $prefix = 'BIL';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    protected $fillable = [
        'ref_no',
        'date',
        'regular_fare',
        'excess_hours_charge',
        'driver_ot_charge',
        'vatable_total',
        'vat',
        'service_total',
        'fuel_charge',
        'meals',
        'tolls',
        'parking',
        'lodging',
        'others',
        'reimbursement_total',
        'total',

        'trip_id',
        'statement_id',

        'approved_by',
        'updated_by',
    ];

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }

    public function statement()
    {
        return $this->belongsTo(Statement::class);
    }


    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('trip', function ($trip) use ($value) {
            return $trip->whereHas('reservation', function ($reservation) use ($value) {
                return $reservation->whereHas('customer', function ($customer) use ($value) {
                    return $customer->where('name', 'LIKE', $value);
                });
            });
        });
    }

    public function getEditUrlAttribute()
    {
        return action('BillController@edit', $this->id);
    }
}
