<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPerson extends Model
{
    protected $fillable = [
        'customer_id',
        'position',
        'primary'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /*
     * Query Scopes
     */

    public function scopeWorksAt($query, $value) {
        return $query->whereHas('customer', function($customer) use ($value) {
            return $customer->whereName($value);
        });
    }

    public function scopePrimary($query, $value = true) {
        return $query->wherePrimary($value);
    }
}
