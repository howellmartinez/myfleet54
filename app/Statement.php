<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Statement extends SerializableFilterableModel
{
    public static $prefix = 'STA';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    protected $fillable = ['date', 'customer_id', 'total', 'ref_no', 'invoice_no', 'updated_by'];

    protected $appends = ['vatable_total', 'vat', 'service_total', 'reimbursement_total', 'serial', 'edit_url'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function transmittal()
    {
        return $this->belongsTo(Transmittal::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /*
     *
     */

    public function getVatableTotalAttribute()
    {
        return $this->bills()->sum('vatable_total');
    }

    public function getServiceTotalAttribute()
    {
        return $this->bills()->sum('service_total');
    }

    public function getVatAttribute()
    {
        return $this->bills()->sum('vat');
    }

    public function getReimbursementTotalAttribute()
    {
        return $this->bills()->sum('reimbursement_total');
    }

    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('customer', function($customer) use ($value) {
            return $customer->where('name', 'LIKE', '%'.$value.'%');
        });
    }

    public function getEditUrlAttribute()
    {
        return action('StatementController@edit', $this->id);
    }
}
