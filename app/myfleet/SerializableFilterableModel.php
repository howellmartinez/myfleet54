<?php
/**
 * Created by PhpStorm.
 * User: hmartinez
 * Date: 16/03/2017
 * Time: 12:49 PM
 */

namespace App\myfleet;

use Illuminate\Database\Eloquent\Model;

abstract class SerializableFilterableModel extends Model
{
    public static $prefix;
    public static $filterableFields;

    protected $appends = ['serial', 'edit_url'];

    public abstract function getEditUrlAttribute();

    public function getSerialAttribute()
    {
        return static::$prefix.'-'.str_pad($this->id, 4, '0', STR_PAD_LEFT);
    }

    public function scopeFilter($query, $filters)
    {
        foreach ($filters as $field => $value) {

            if (in_array($field, array_pluck(static::$filterableFields, 'name'))) {
                $scope_method = camel_case($field);
                $method_name = 'scope' . camel_case($field);
                if (method_exists($this, $method_name)) {
                    $query = $query->$scope_method($value);
                } else {
                    $query = $query->where($field, 'LIKE', '%'.$value.'%');
                }
            }
        }
        return $query;
    }
}