<?php

namespace App\myfleet;

class Errors
{

    protected static $errors = [
        // Customers
        'payments_customer_id_foreign' => 'Cannot delete a customer with existing payments.',

        'reservations_authorizer_id_foreign' => 'Cannot delete authorizer for a reservation.',
    ];

    // Takes in errorMessage
    // Returns error item if key is found in the errors
    public static function findError($message)
    {
        return array_first(self::$errors, function ($value, $key) use ($message) {
            return str_contains($message, $key);
        });
    }
}

