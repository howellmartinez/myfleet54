<?php
/**
 * Created by PhpStorm.
 * User: hmartinez
 * Date: 06/03/2017
 * Time: 7:39 PM
 */

namespace App;


trait Persistable
{
    protected $model = null;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function foo($model)
    {
        if($this->model != null) {
            $this->model->update($this->all());
        } else {
            $new = new $this->model($this->all());
            $new->save();
        }
    }
}