<?php

namespace App\Repositories;

abstract class EloquentRepository
{
    protected $model;

    function __construct($model)
    {
        $this->model = $model;
    }

    public function leave($attributes)
    {
        return new $this->model($attributes);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function get()
    {
        return $this->model->get();
    }

    public function with($relationships = [])
    {
        return $this->model->with($relationships);
    }
}