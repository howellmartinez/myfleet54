<?php

namespace App\Repositories;

use App\myfleet\EloquentFilterableTrait;
use App\Payment;

class Payments extends EloquentRepository
{
    use EloquentFilterableTrait;

    function __construct(Payment $model)
    {
        parent::__construct($model);
        $this->filterableFields = ['id', 'customer'];
        $this->filterModel = $this->model;
    }

    public function filterByCustomerName($value)
    {
        return $this->filterModel->whereHas('customer', function($customer) use ($value) {
            return $customer->whereName($value);
        });
    }

}