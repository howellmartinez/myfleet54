<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Trip extends SerializableFilterableModel
{
    public static $prefix = 'TRI';

    public static $filterableFields = [
        ['label' => 'Customer', 'name' => 'customer']
    ];

    protected $fillable = [
        'completed',
        'ticket_no',
        'date',
        'type',
        'location',
        'destination',
        'pickup_place',
        'petty_cash',
        'passenger_contact',
        'pickup_time',
        'notes',

        'car_id',
        'driver_id',
        'reservation_id',
        'passenger_id',
    ];

    public function legs()
    {
        return $this->hasMany(Leg::class);
    }

    public function bill()
    {
        return $this->hasOne(Bill::class);
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function passenger()
    {
        return $this->belongsTo(Person::class, 'passenger_id');
    }


    /*
     * Query Scopes
     */

    public function scopeBilled($query)
    {
        return $query->has('bill');
    }

    public function scopeUnbilled($query)
    {
        return $query->doesnthave('bill');
    }

    public function scopeComplete($query)
    {
        return $query->whereCompleted(true);
    }

    public function scopeIncomplete($query)
    {
        return $query->whereCompleted(false);
    }

    /*
     * Query Scopes
     */
    public function scopeCustomer($query, $value)
    {
        return $query->whereHas('reservation', function ($reservation) use ($value) {
            return $reservation->whereHas('customer', function ($customer) use ($value) {
                return $customer->where('name', 'LIKE', $value);
            });
        });
    }

    public function getEditUrlAttribute()
    {
        return action('TripController@edit', $this->id);
    }
}
