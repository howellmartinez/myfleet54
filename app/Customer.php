<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Customer extends SerializableFilterableModel
{
    public function getEditUrlAttribute() {
        return action('CustomerController@edit', $this->id);
    }

    public static $prefix = 'CUS';

    public static $filterableFields = [
        ['label' => 'Name', 'name' => 'name'],
        ['label' => 'Code', 'name' => 'code'],
        ['label' => 'Group', 'name' => 'group'],
        ['label' => 'VAT', 'name' => 'vat'],
        ['label' => 'MFI Code', 'name' => 'mfi_code'],
        ['label' => 'Primary person', 'name' => 'primary_person'],
    ];

    public $casts = [
        'vat' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'code',
        'vat',
        'mfi_code',
        'tin',
        'terms',
        'address',
        'notes',
        'customer_group_id',
    ];

    public function customerPersons()
    {
        return $this->hasMany(CustomerPerson::class);
    }

    public function persons()
    {
        return $this->belongsToMany(Person::class)->withPivot(['department', 'position']);
    }

    public function customerGroup()
    {
        return $this->belongsTo(CustomerGroup::class);
    }

    public function primaryPerson()
    {
        return $this->persons()->wherePivot('is_primary', true);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function trips()
    {
        return $this->hasManyThrough(Trip::class, Reservation::class);
    }

    public function billedTrips()
    {
        return $this->trips()->billed();
    }

    public function statements()
    {
        return $this->hasMany(Statement::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }

    /*
     * Queried Relationships
     */

    public function unreceiptedPayments()
    {
        return $this->payments()->unreceipted();
    }

    /*
     * Query Scopes
     */

    public function scopePrimaryPerson($query, $value)
    {
        return $query->whereHas('primaryPerson', function($person) use ($value) {
            return $person
                ->select('id', \DB::raw('CONCAT_WS(" ", first_name, last_name) as name'))
                ->groupBy('id')
                ->having('name', 'LIKE', '%'.$value.'%');
        });
    }
}
