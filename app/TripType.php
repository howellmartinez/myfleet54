<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripType extends Model
{
    protected $fillable = ['name', 'hours', 'rate'];

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
