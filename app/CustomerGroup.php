<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    protected $fillable = ['name', 'code', 'updated_by'];

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
