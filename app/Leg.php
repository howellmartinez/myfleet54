<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Leg extends Model
{
    public $fillable = [
        'origin', 'destination', 'km_start', 'km_end',
        'datetime_start', 'datetime_end', 'fuel_start', 'fuel_end',
        'updated_by', 'order'
    ];

    public $timestamps = false;

    public $dates = ['datetime_start', 'datetime_end'];

    public function setDatetimeEndAttribute($value)
    {
        $this->attributes['datetime_end'] = Carbon::parse($value);
    }

    public function getDatetimeEndAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function setDatetimeStartAttribute($value)
    {
        $this->attributes['datetime_start'] = Carbon::parse($value);
    }

    public function getDatetimeStartAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }
}
