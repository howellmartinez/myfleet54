<?php

namespace App;


trait Serializable
{
    public abstract function getPrefixAttribute();
    public abstract function getLinkAttribute();

    public function getSerialAttribute()
    {
        return $this->prefix.'-'.str_pad($this->id, 4, '0', STR_PAD_LEFT);
    }
}