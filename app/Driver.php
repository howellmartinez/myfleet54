<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Driver extends SerializableFilterableModel
{
    public static $prefix = 'DRI';

    public $timestamps = false;

    public static $filterableFields = [
        ['label' => 'First Name', 'name' => 'first_name']
    ];

    protected $fillable = ['first_name', 'last_name', 'middle_name', 'updated_by', 'notes'];

    protected $appends = ['full_name', 'serial', 'edit_url'];

    public function person()
    {
        return $this->belongsTo(Driver::class);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getEditUrlAttribute()
    {
        return action('DriverController@edit', $this->id);
    }
}
