<?php

namespace App;

use App\myfleet\SerializableFilterableModel;

class Car extends SerializableFilterableModel
{
    public static $prefix = 'CAR';

    public static $filterableFields = [
        ['label' => 'Type', 'name' => 'type']
    ];

    protected $fillable = [
        'type',
        'class',
        'make',
        'model',
        'fuel',
        'tx',
        'year',
        'color',
        'max_cap',
        'plate',
        'conduction_no',
        'engine_no',
        'serial_no',
        'or_no',
        'cr_no',
        'registered_owner',
        'status',
    ];

    public function getEditUrlAttribute()
    {
        return action('CarController@edit', $this->id);
    }
}
