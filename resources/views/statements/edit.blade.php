@extends('layouts.app')

@section('content')

    <a href="{!! action('StatementController@index') !!}">Back to Statements</a>

    <br><br>

    <div id="statement" v-cloak>

        <div class="pull-right">
            <delete-button :is-busy="statement.isBusy" :is-deleting="statement.isDeleting" @destroy="destroy">
        </div>

        <h2>Edit Statement</h2>

        <br>

        <form @submit.prevent="update">
            @include('statements._form')
        </form>
    </div>

@endsection