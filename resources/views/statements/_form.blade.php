{!! csrf_field() !!}

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label">Customer</label>
            <select class="form-control" v-model="selectedCustomer">
                <option v-for="customer in customers" :value="customer">
                    @{{ customer.name }}
                </option>
            </select>
        </div>
    </div>

    <div class="offset-1 col-md-3">
        <div class="form-group">
            <label class="control-label">Date</label>
            <input type="date" class="form-control" v-model="statement.date">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Billing Invoice No.</label>
            <input type="text" class="form-control" placeholder="Billing Invoice No."
                   v-model="statement.invoice_no">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Ref No.</label>
            <input type="text" class="form-control" placeholder="Ref No."
                   v-model="statement.ref_no">
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">Billed Trips</div>
    <div class="card-block">
        <table class="table small table-responsive table-bordered table-compact">
            <thead>
            <tr>
                <th></th>
                <th>Ticket #</th>
                <th>Charging Loc.</th>
                <th>Date</th>
                <th>Passenger</th>
                <th>Vehicle</th>
                <th class="text-right">Start KM</th>
                <th class="text-right">End KM</th>
                <th class="text-right">Unit Price</th>
                <th class="text-right">Excess Hr.</th>
                <th class="text-right">Driver OT</th>
                <th class="text-right">EVAT</th>
                <th class="text-right">Fuel</th>
                <th class="text-right">Tolls</th>
                <th class="text-right">Parking</th>
                <th class="text-right">Meals</th>
                <th class="text-right">Lodging</th>
                <th class="text-right">Others</th>
                <th class="text-right">Total</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="trip in customerTrips">
                <td><input type="checkbox" v-model="statement.bills" :value="trip.bill.id"></td>
                <td>@{{ trip.ticket_no }}</td>
                <td>@{{ trip.reservation.charging_loc }}</td>
                <td>@{{ trip.date }}</td>
                <td>@{{ trip.passenger.full_name }}</td>
                <td>@{{ trip.car.type }}</td>
                <td class="text-right">TODO</td>
                <td class="text-right">TODO</td>
                <td class="text-right">TODO</td>
                <td class="text-right">@{{ trip.bill.meals }}</td>
                <td class="text-right">@{{ trip.bill.driver_ot_charge }}</td>
                <td class="text-right">@{{ trip.bill.vat }}</td>
                <td class="text-right">@{{ trip.bill.fuel_charge }}</td>
                <td class="text-right">@{{ trip.bill.tolls }}</td>
                <td class="text-right">@{{ trip.bill.parking }}</td>
                <td class="text-right">@{{ trip.bill.meals }}</td>
                <td class="text-right">@{{ trip.bill.lodging }}</td>
                <td class="text-right">@{{ trip.bill.others }}</td>
                <td class="text-right">@{{ trip.bill.total }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<br>

<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-block">
                Approved by:
            </div>
        </div>
    </div>

    <div class="col-8">
        {{--Fees--}}
        <div class="card">
            <div class="card-block small">
                <div class="row">

                    {{--Service Rendered--}}
                    <div class="col-6" style="border-right: 1px solid #ccc;">
                        <strong>A. Service Rendered:</strong>
                        <br><br>
                        <dl class="row">

                            <dd class="offset-1 col-5">Vatable Sales</dd>
                            <dd class="col-6 text-right">@{{ vatableTotal.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Less: W/Tax</dd>
                            <dd class="col-6 text-right">xxx</dd>

                            <dd class="offset-1 col-5">Add: VAT</dd>
                            <dd class="col-6 text-right">@{{ vat.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Total Amount Due:</dd>
                            <dd class="col-6 text-right">@{{ serviceTotal.toFixed(2) }}</dd>
                        </dl>
                    </div>

                    {{--Reimbursement Charges--}}
                    <div class="col-6">
                        <strong>B. Reimbursement Charges:</strong>
                        <br><br>
                        <dl class="row">

                            <dd class="offset-1 col-5">Fuel</dd>
                            <dd class="col-6 text-right">@{{ fuelCharge.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Toll & Parking</dd>
                            <dd class="col-6 text-right">@{{ tolls.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Meals</dd>
                            <dd class="col-6 text-right">@{{ meals.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Lodging:</dd>
                            <dd class="col-6 text-right">@{{ lodging.toFixed(2) }}</dd>

                            <dd class="offset-1 col-5">Total:</dd>
                            <dd class="col-6 text-right">@{{ reimbursementTotal.toFixed(2) }}</dd>
                        </dl>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<hr>

<save-button :is-busy="statement.isBusy" :is-saving="statement.isSaving"></save-button>

@push('scripts')
<script src="/js/statement.js"></script>
@endpush