@extends('layouts.app')

@section('content')

    <a href="{!! action('StatementController@index') !!}">Back to Statements</a>

    <br><br>

    <h2>Create Statement</h2>

    <br>

    <div id="statement" v-cloak>
        <form @submit.prevent="store">
            @include('statements._form')
        </form>
    </div>
@endsection