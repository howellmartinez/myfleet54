@extends('layouts.app')

@section('content')

    <div id="trip" v-cloak>

        <a href="{!! action('TripController@index') !!}">Back to Trips</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="trip.isBusy" :is-deleting="trip.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Trip</h2>

        <br>

        <form @submit.prevent="update">
            @include('trips._form')
            <timestamp :name="trip.updated_by" :time="trip.updated_at"></timestamp>
        </form>
    </div>

@endsection