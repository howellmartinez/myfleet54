<div class="card">
    <div class="card-header">
        Pending Reservations
    </div>
    <div class="card-block">
        <table class="table table-hover">
            <thead>
            <tr>
                <th></th>
                <th>PO#</th>
                <th>Trip Date</th>
                <th>Customer</th>
                <th>Trip Type</th>
                <th>Destination</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="reservation in reservations"
                v-on:click="selectedReservation = reservation; reservationChanged(selectedReservation)"
                :class="{ 'bg-info': selectedReservation.id == reservation.id }">
                <td>
                    <input type="radio" name="reservation"
                           v-on:change="reservationChanged(selectedReservation)"
                           v-model="selectedReservation"
                           :value="reservation">
                </td>
                <td>@{{ reservation.id }}</td>
                <td>@{{ reservation.trip_date }}</td>
                <td>@{{ reservation.customer.name }}</td>
                <td>@{{ reservation.type }}</td>
                <td>@{{ reservation.destination }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<br>

<div class="card">
    <div class="card-header">
        Trip Info
        <div class="float-right">
            <input type="checkbox" v-model="trip.completed"> Completed
        </div>
    </div>
    <div class="card-block">
        <div class="row">

            <div class="col-6" style="border-right: #eeeeee solid 1px">
                <div class="form-group row" :class="{'has-danger': trip.errors.has('date')}">
                    <label class="col-4 col-form-label form-control-feedback">Date</label>
                    <div class="col-8">
                        <input type="date" class="form-control"
                               :disabled="!trip.completed"
                               v-model="trip.date">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('date')">
                            @{{ trip.errors.get('date') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('ticket_no')}">
                    <label class="col-4 col-form-label form-control-feedback">Trip Ticket No.</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Trip Ticket No."
                           v-model="trip.ticket_no">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('ticket_no')">
                            @{{ trip.errors.get('ticket_no') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('driver_id')}">
                    <label class="col-4 col-form-label form-control-feedback">Driver</label>
                    <div class="col-8">
                        <select class="form-control"
                                v-model="trip.driver_id">
                            <option v-for="driver in drivers" :value="driver.id">@{{ driver.full_name }}</option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('driver_id')">
                            @{{ trip.errors.get('driver_id') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('car_id')}">
                    <label class="col-4 col-form-label form-control-feedback">Car</label>
                    <div class="col-8">
                        <select class="form-control"
                                v-model="trip.car_id">
                            <option v-for="car in cars" :value="car.id">@{{ car.serial }}</option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('car_id')">
                            @{{ trip.errors.get('car_id') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('passenger_id')}">
                    <label class="col-4 col-form-label form-control-feedback">Passenger</label>
                    <div class="col-8">
                        <select class="form-control"
                                :disabled="!trip.completed"
                                v-model="trip.passenger_id">
                            <option v-for="person in persons" :value="person.id">
                                @{{ person.full_name }}
                            </option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('passenger_id')">
                            @{{ trip.errors.get('passenger_id') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('passenger_contact')}">
                    <label class="col-4 col-form-label form-control-feedback">Contact No.</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Contact No."
                               v-model="trip.passenger_contact">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('passenger_contact')">
                            @{{ trip.errors.get('passenger_contact') }}
                        </small>
                    </div>

                </div>
            </div>

            <div class="col-6">
                <div class="form-group row" :class="{'has-danger': trip.errors.has('type')}">
                    <label class="col-4 col-form-label form-control-feedback">Type</label>
                    <div class="col-8">
                        <select class="form-control"
                                :disabled="!trip.completed"
                                v-model="trip.type">
                            <option v-for="trip_type in trip_types">
                                @{{ trip_type }}
                            </option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('type')">
                            @{{ trip.errors.get('type') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('location')}">
                    <label class="col-4 col-form-label form-control-feedback">Trip Location</label>
                    <div class="col-8">
                        <select class="form-control"
                                :disabled="!trip.completed"
                                v-model="trip.location">
                            <option v-for="location in locations">
                                @{{ location }}
                            </option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('location')">
                            @{{ trip.errors.get('location') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('destination')}">
                    <label class="col-4 col-form-label form-control-feedback">Destination</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Destination"
                               :disabled="!trip.completed"
                               v-model="trip.destination">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('destination')">
                            @{{ trip.errors.get('destination') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('pickup_place')}">
                    <label class="col-4 col-form-label form-control-feedback">Pick-up Place</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Pick-up Place"
                               :disabled="!trip.completed"
                               v-model="trip.pickup_place">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('pickup_place')">
                            @{{ trip.errors.get('pickup_place') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('pickup_time')}">
                    <label class="col-4 col-form-label form-control-feedback">Pick-up Time</label>
                    <div class="col-8">
                        <input type="time" class="form-control" placeholder="Pick-up Time"
                               :disabled="!trip.completed"
                               v-model="trip.pickup_time">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('pickup_time')">
                            @{{ trip.errors.get('pickup_time') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row" :class="{'has-danger': trip.errors.has('petty_cash')}">
                    <label class="col-4 col-form-label form-control-feedback">Petty Cash</label>
                    <div class="col-8">
                        <input type="number" class="form-control" placeholder="0.00"
                               v-model.number="trip.petty_cash">
                        <small class="form-text form-control-feedback" v-if="trip.errors.has('petty_cash')">
                            @{{ trip.errors.get('petty_cash') }}
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div v-if="trip.completed">
    <div class="card">
        <div class="card-header">
            Trip Legs
            <div class="close">
                <i class="fa fa-plus-circle" @click="addLeg(trip)"></i>
            </div>
        </div>
        <div class="card-block">
            <table class="table grid">
                <thead>
                <tr class="row m-0">
                    <th class="d-inline-block col-1">Origin</th>
                    <th class="d-inline-block col-2">Destination</th>
                    <th class="d-inline-block col-2">Start</th>
                    <th class="d-inline-block col-2">End</th>

                    <th class="d-inline-block col-1">Km Start</th>
                    <th class="d-inline-block col-1">Km End</th>
                    <th class="d-inline-block col-1">Fuel Start</th>
                    <th class="d-inline-block col-1">Fuel End</th>
                    <th class="d-inline-block col-1"></th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(leg, index) in trip.legs" class="row m-0">
                    <td class="d-inline-block col-1"><input type="text" class="rounded-0 form-control"
                                                            placeholder="Origin" v-model="leg.origin"></td>
                    <td class="d-inline-block col-2"><input type="text" class="rounded-0 form-control"
                                                            placeholder="Destination" v-model="leg.destination">
                    </td>
                    <td class="d-inline-block col-2"><input type="datetime-local" class="rounded-0 form-control"
                                                            placeholder="Start" v-model="leg.datetime_start">
                    </td>
                    <td class="d-inline-block col-2"><input type="datetime-local" class="rounded-0 form-control"
                                                            placeholder="End" v-model="leg.datetime_end"></td>
                    <td class="d-inline-block col-1"><input type="number" class="rounded-0 form-control"
                                                            placeholder="Km Start" v-model="leg.km_start"></td>
                    <td class="d-inline-block col-1"><input type="number" class="rounded-0 form-control"
                                                            placeholder="Km End" v-model="leg.km_end"></td>
                    <td class="d-inline-block col-1"><input type="number" class="rounded-0 form-control"
                                                            placeholder="Fuel Start" v-model="leg.fuel_start">
                    </td>
                    <td class="d-inline-block col-1">
                        <input type="number" class="rounded-0 form-control" placeholder="Fuel End"
                               v-model="leg.fuel_end">
                    </td>
                    <td class="d-inline-block col-1 vert-align">
                        <button class="btn btn-sm btn-link" type="button" @click="trip.legs.splice(index, 1)">
                        <i class="fa fa-times-circle"></i>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<hr>

{{--<input type="text" name="daterange" value="01/01/2015 - 01/31/2015"/>--}}

<save-button :is-busy="trip.isBusy" :is-saving="trip.isSaving"></save-button>

{{--@push('styles')--}}
{{--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>--}}
{{--@endpush--}}

@push('scripts')
{{--<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>--}}
{{--<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>--}}
{{--<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>--}}
<script src="/js/trip.js"></script>
{{--<script type="text/javascript">--}}
{{--$(function() {--}}
{{--$('input[name="daterange"]').daterangepicker();--}}
{{--});--}}
{{--</script>--}}
@endpush