@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

        <a href="{!! action('TripController@create') !!}">Create Trip</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Trips</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Car</th>
                    <th>Customer</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.date }}</td>
                    <td>@{{ item.car.serial }}</td>
                    <td>@{{ item.reservation.customer.name }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>

@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush