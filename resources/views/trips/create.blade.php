@extends('layouts.app')

@section('content')

    <div id="trip" v-cloak>

        <a href="{!! action('TripController@index') !!}">Back to Trips</a>

        <br><br>

        <h2>Create Trip</h2>

        <br>

        <form @submit.prevent="store">
            @include('trips._form')
        </form>
    </div>

@endsection