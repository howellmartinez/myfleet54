@extends('layouts.app')

@section('content')

    <div id="payment">
        <a href="{!! action('PaymentController@index') !!}">Back to Payments</a>

        <br><br>

        <div class="pull-right">
            <delete-button :is-busy="payment.isBusy" :is-deleting="payment.isDeleting" @destroy="destroy">
        </div>

        <h2>Edit Payment</h2>

        <br>

        <form @submit.prevent="update">
            @include('payments._form')
        </form>
    </div>
@stop