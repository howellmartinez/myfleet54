{!! csrf_field() !!}

<div class="row">
    <div class="col-md-6">
        <div class="card">

            <div class="card-header">Payment Info</div>

            <div class="card-block">
                <div class="form-group">
                    <label class="control-label">Ref No</label>
                    <input name="date" type="ref_no" class="form-control" v-model="payment.ref_no">
                </div>

                <div class="form-group">
                    <label class="control-label">Date</label>
                    <input name="date" type="date" class="form-control" v-model="payment.date">
                </div>

                <div class="form-group">
                    <label class="control-label">Amount</label>
                    <input name="amount" type="number" class="form-control" placeholder="0.00" v-model="payment.amount">
                </div>

                <div class="form-group">
                    <label class="control-label">Statement</label>
                    <select class="form-control" v-model="selectedStatement">
                        <option v-for="statement in customerStatements" :value="statement">
                            @{{ statement.ref_no }}
                        </option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Customer</label>
                    <select class="form-control" v-model="selectedCustomer">
                        <option v-for="customer in customers" :value="customer">
                            @{{ customer.name }}
                        </option>
                    </select>
                </div>

            </div>
        </div>
    </div>

</div>

<hr>

<save-button :is-busy="payment.isBusy" :is-saving="payment.isSaving"></save-button>

@push('scripts')
<script src="/js/payment.js"></script>
@endpush