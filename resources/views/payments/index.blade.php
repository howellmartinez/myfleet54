@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>
        <a href="{!! action('PaymentController@create') !!}">Create Payment</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">
            <slot>
                <div slot="header">Payments</div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ref No.</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th>Statement</th>
                        <th class="text-right">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                        <td>@{{ item.ref_no }}</td>
                        <td>@{{ item.date }}</td>
                        <td>@{{ item.customer.name }}</td>
                        <td><a :href="'/app/statements/' + item.statement_id + '/edit'">@{{ item.statement.ref_no }}</a>
                        </td>
                        <td class="text-right">@{{ item.amount }}</td>
                    </tr>
                    </tbody>
                </table>
            </slot>
        </index>
    </div>
@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush