@extends('layouts.app')

@section('content')

    <a href="{!! action('PaymentController@index') !!}">Back to Payments</a>

    <br><br>

    <h2>Create Payment</h2>

    <br>

    <div id="payment">
        <form @submit.prevent="store">
            @include('payments._form')
        </form>
    </div>
@stop