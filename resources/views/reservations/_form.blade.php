{!! csrf_field() !!}

<div class="row">

    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Client Info</div>
            <div class="card-block">
                <div class="form-group" :class="{'has-danger': reservation.errors.has('date')}">
                    <label class="control-label form-control-feedback">Reservation Date</label>
                    <input name="date" type="date" class="form-control"
                           v-model="reservation.date">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('date')">
                        @{{ reservation.errors.get('date') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('customer_id')}">
                    <label class="control-label form-control-feedback">Customer</label>
                    <select name="customer_id" id="customer_id" class="form-control" v-model="reservation.customer_id">
                        <option v-for="customer in customers" :value="customer.id">
                            @{{ customer.name }}
                        </option>
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('customer_id')">
                        @{{ reservation.errors.get('customer_id') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('department')}">
                    <label class="control-label form-control-feedback">Department</label>
                    <input name="department" type="text" class="form-control" placeholder="Department"
                           v-model="reservation.department">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('department')">
                        @{{ reservation.errors.get('department') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('requestor_id')}">
                    <label class="control-label form-control-feedback">Requestor</label>
                    <select name="requestor_id" class="form-control" v-model="reservation.requestor_id">
                        <option v-for="person in persons" :value="person.id">
                            @{{ person.full_name }}
                        </option>
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('requestor_id')">
                        @{{ reservation.errors.get('requestor_id') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('authorizer_id')}">
                    <label class="control-label form-control-feedback">Authorizer</label>
                    <select name="authorizer_id" class="form-control" v-model="reservation.authorizer_id">
                        <option v-for="person in persons" :value="person.id">
                            @{{ person.full_name }}
                        </option>
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('authorizer_id')">
                        @{{ reservation.errors.get('authorizer_id') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('passenger_id')}">
                    <label class="control-label form-control-feedback">Passenger</label>
                    <select name="passenger_id" class="form-control" v-model="selectedPassenger">
                        <option v-for="person in persons" :value="person">
                            @{{ person.full_name }}
                        </option>
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('passenger_id')">
                        @{{ reservation.errors.get('passenger_id') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('passenger_contact')}">
                    <label class="control-label form-control-feedback">Contact</label>
                    <input name="passener_contact" type="text" class="form-control"
                           v-model="reservation.passenger_contact">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('passenger_contact')">
                        @{{ reservation.errors.get('passenger_contact') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('charging_loc')}">
                    <label class="control-label form-control-feedback">Charging Location</label>
                    <input name="charging_loc" type="text" class="form-control" placeholder="Charging Location"
                           v-model="reservation.charging_loc">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('charging_loc')">
                        @{{ reservation.errors.get('charging_loc') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('purpose')}">
                    <label class="control-label form-control-feedback">Purpose</label>
                    <input name="purpose" type="text" class="form-control" placeholder="Purpose"
                           v-model="reservation.purpose">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('purpose')">
                        @{{ reservation.errors.get('purpose') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('ref_no')}">
                    <label class="control-label form-control-feedback">Ref. No.</label>
                    <input name="ref_no" type="text" class="form-control" placeholder="Ref. No."
                           v-model="reservation.ref_no">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('ref_no')">
                        @{{ reservation.errors.get('ref_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('po_document_path')}">
                    <label class="control-label form-control-feedback">PO Document</label>
                    <input name="po_document_path" type="file" class="form-control">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('po_document_path')">
                        @{{ reservation.errors.get('po_document_path') }}
                    </small>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">

            <div class="card-header">Trip Info</div>

            <div class="card-block">

                <div class="form-group" :class="{'has-danger': reservation.errors.has('trip_date')}">
                    <label class="control-label form-control-feedback">Trip Date</label>
                    <input name="trip_date" type="date" class="form-control"
                           v-model="reservation.trip_date">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('trip_date')">
                        @{{ reservation.errors.get('trip_date') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('trip_type_id')}">
                    <label class="control-label form-control-feedback">Trip Type</label>
                    <select name="type" class="form-control" v-model="reservation.trip_type_id">
                        <option v-for="tripType in tripTypes">@{{ tripType.name }}</option>
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('trip_type_id')">
                        @{{ reservation.errors.get('trip_type_id') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('location')}">
                    <label class="control-label form-control-feedback">Trip Location</label>
                    <select name="location" class="form-control"
                            v-model="reservation.location">
                        @foreach(config('myfleet.trip_locations') as $trip_location)
                            <option>{{ $trip_location }}</option>
                        @endforeach
                    </select>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('location')">
                        @{{ reservation.errors.get('location') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('vehicle')}">
                    <label class="control-label form-control-feedback">Vehicle Requested</label>
                    <input name="vehicle" type="text" class="form-control" placeholder="Vehicle Requested"
                           v-model="reservation.vehicle">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('vehicle')">
                        @{{ reservation.errors.get('vehicle') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('pickup_place')}">
                    <label class="control-label form-control-feedback">Pickup Place</label>
                    <input name="pickup_place" type="text" class="form-control" placeholder="Pickup Place"
                           v-model="reservation.pickup_place">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('pickup_place')">
                        @{{ reservation.errors.get('pickup_place') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('pickup_time')}">
                    <label class="control-label form-control-feedback">Pickup Time</label>
                    <input name="pickup_time" type="time" class="form-control" placeholder="Pickup Time"
                           v-model="reservation.pickup_time">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('pickup_time')">
                        @{{ reservation.errors.get('pickup_time') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('destination')}">
                    <label class="control-label form-control-feedback">Destination</label>
                    <input name="destination" type="text" class="form-control" placeholder="Destination"
                           v-model="reservation.destination">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('destination')">
                        @{{ reservation.errors.get('destination') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('trip_end')}">
                    <label class="control-label form-control-feedback">Date/Time End</label>
                    <input name="trip_end" type="datetime-local" class="form-control"
                           v-model="reservation.trip_end">
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('trip_end')">
                        @{{ reservation.errors.get('trip_end') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': reservation.errors.has('notes')}">
                    <label class="control-label form-control-feedback">Other Instructions</label>
                    <textarea name="notes" class="form-control" v-model="reservation.notes"
                              placeholder="Other Instructions"></textarea>
                    <small class="form-text form-control-feedback" v-if="reservation.errors.has('notes')">
                        @{{ reservation.errors.get('notes') }}
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<save-button :is-busy="reservation.isBusy" :is-saving="reservation.isSaving"></save-button>

@push('scripts')
<script src="/js/reservation.js"></script>
<script>
    $(document).ready(function () {
        $("[name=customer_id]").select2({
            theme: "bootstrap"
        });
    });
</script>

@endpush