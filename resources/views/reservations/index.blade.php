@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>
        <a href="{!! action('ReservationController@create') !!}">Create Reservation</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Reservations</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>PO #</th>
                    <th>Res. Date</th>
                    <th>Trip Date</th>
                    <th>Company</th>
                    <th>Vehicle Req.</th>
                    <th>Pickup Place</th>
                    <th>Destination</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.ref_no }}</td>
                    <td>@{{ item.date }}</td>
                    <td>@{{ item.trip_date }}</td>
                    <td>@{{ item.customer.name }}</td>
                    <td>@{{ item.vehicle }}</td>
                    <td>@{{ item.pickup_place }}</td>
                    <td>@{{ item.destination }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>
@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush