@extends('layouts.app')

@section('content')

    <div id="reservation" v-cloak>

        <a href="{!! action('ReservationController@index') !!}">Back to Reservations</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="reservation.isBusy" :is-deleting="reservation.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Reservation</h2>

        <br>
        <form @submit.prevent="update" v-on:keydown="reservation.errors.clear($event.target.name)">
            @include('reservations._form')
            <timestamp :name="reservation.updated_by" :time="reservation.updated_at"></timestamp>
        </form>
    </div>
@stop