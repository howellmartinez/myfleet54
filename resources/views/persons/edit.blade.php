@extends('layouts.app')

@section('content')

    <a href="{!! action('PersonController@index') !!}">Back to Persons</a>

    <br><br>

    <div id="person" v-cloak>

        <div class="float-right">
            <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Person</h2>

        <br>

        <form @submit.prevent="update" @keydown="form.errors.clear($event.target.name)">
            @include('persons._form')
            <timestamp :name="form.updated_by" :time="form.updated_at"></timestamp>
        </form>
    </div>

@endsection