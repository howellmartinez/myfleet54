@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

    <a href="{!! action('PersonController@create') !!}">Register Person</a>

    <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Persons</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Nickname</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.last_name }}</td>
                    <td>@{{ item.first_name }}</td>
                    <td>@{{ item.nickname }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>

@endsection

@push('scripts')
<script src="/js/index.js"></script>
@endpush