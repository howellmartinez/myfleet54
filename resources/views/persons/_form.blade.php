{!! csrf_field() !!}

<div class="row">

    <div class="col-2">
        <select class="form-control" v-model="newTabType">
            <option v-for="tabType in tabTypes">@{{ tabType }}</option>
        </select>
    </div>

    <div class="col-2">
        <button type="button" class="btn btn-primary" @click="addTab">
        <i class="fa fa-plus-circle"></i> Add Type
        </button>
    </div>

</div>

<br><br>

<div class="card">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            {{-- General Tab --}}
            <li class="nav-item">
                <a class="nav-link" href="#"
                   :class="{active: currentTab=='General'}"
                   v-on:click="currentTab = 'General'">
                    General Info
                </a>
            </li>

            {{-- Customer Tabs --}}
            <li class="nav-item" v-for="(customer, index) in form.customers">
                <a class="nav-link" href="#"
                   :class="{active: currentTab == 'Customer ' + (index + 1) }"
                   v-on:click="currentTab = 'Customer ' + (index + 1)">
                    Customer @{{ index + 1 }}
                </a>
            </li>

            {{-- Driver Tabs --}}
            <li class="nav-item" v-for="(driver, index) in form.drivers">
                <a class="nav-link" href="#"
                   :class="{active: currentTab == 'Driver ' + (index + 1) }"
                   v-on:click="currentTab = 'Driver ' + (index + 1)">
                    Driver @{{ index + 1 }}
                </a>
            </li>
        </ul>
    </div>

    <div class="card-block" v-show="currentTab == 'General'">

        <h2>General Info</h2>

        <div class="form-group" :class="{'has-danger': form.errors.has('last_name')}">
            <label class="control-label form-control-feedback">Last Name</label>
            <input name="last_name" type="text" class="form-control" placeholder="Last Name"
                   v-model="form.last_name">
            <small class="form-text form-control-feedback" v-if="form.errors.has('last_name')">
                @{{ form.errors.get('last_name') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('first_name')}">
            <label class="control-label form-control-feedback">First Name</label>
            <input name="first_name" type="text" class="form-control" placeholder="First Name"
                   v-model="form.first_name">
            <small class="form-text form-control-feedback" v-if="form.errors.has('first_name')">
                @{{ form.errors.get('first_name') }}
            </small>
        </div>

        <div class="form-group">
            <label class="control-label">Middle Name
                <small class="text-muted">(Optional)</small>
            </label>
            <input name="middle_name" type="text" class="form-control" placeholder="Last Name"
                   v-model="form.middle_name">
        </div>

        <div class="form-group">
            <label class="control-label">Nickname
                <small class="text-muted">(Optional)</small>
            </label>
            <input name="nickname" type="text" class="form-control" placeholder="Nickname"
                   v-model="form.nickname">
        </div>

        <field-list
                :name="'phone'"
                :list="form.phones"
                :labels="{{ json_encode(config('myfleet.phones')) }}"
        ></field-list>

        <br>

        <field-list
                :name="'email'"
                :list="form.emails"
                :labels="{{ json_encode(config('myfleet.emails')) }}"
        ></field-list>

        <br>

        <field-list
                :name="'relationship'"
                :list="form.relationships"
                :labels="{{ json_encode(config('myfleet.relationships')) }}"
        ></field-list>

        <br>

        <field-list
                :name="'other'"
                :list="form.others"
                :labels="{{ json_encode('') }}"
        ></field-list>

        <br>

        <div class="form-group">
            <label class="control-label">Notes
                <small class="text-muted">(Optional)</small>
            </label>
            <textarea class="form-control" placeholder="Notes" v-model="form.notes"></textarea>
        </div>
    </div>

    {{--Customer Form--}}
    <template v-for="(customer, index) in form.customers">
        <div class="card-block" v-show="currentTab == 'Customer ' + (index + 1)">

            <h2>Customer @{{ index + 1 }} Info</h2>

            @include('persons.customer_people._form')

        </div>
    </template>

    {{--Driver Form--}}
    <template v-for="(driver, index) in form.drivers">
        <div class="card-block" v-show="currentTab == 'Driver ' + (index + 1)">

            <h2>Driver @{{ index + 1 }} Info</h2>

            @include('persons.drivers._form')
        </div>
    </template>

</div>

<hr>

<save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>

@push('scripts')
<script src="/js/person.js"></script>
@endpush