<div class="form-group">
    <label class="control-label">Notes
        <small class="text-muted">(Optional)</small>
    </label>
    <textarea name="notes" class="form-control" placeholder="Notes" v-model="driver.notes"></textarea>
</div>

