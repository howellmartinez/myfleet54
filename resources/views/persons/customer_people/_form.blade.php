<div class="form-group">
    <label class="control-label">Customer</label>
    <div class="row">
        <div class="col-md-9">
            <select name="customer_id" class="form-control" v-model="customer.pivot.customer_id">
                <option v-for="customer in customers" :value="customer.id">@{{ customer.name }}</option>
            </select>
        </div>
        <div class="col-md-3">
            {{--<button class="btn btn-block" type="button"--}}
                    {{--v-on:click="customer.primary = !customer.primary"--}}
                    {{--:class="{'btn-secondary': !customer.primary, 'btn-info': customer.primary, }">--}}
                {{--<i class="fa"--}}
                   {{--:class="{'fa-square-o': !customer.primary, 'fa-check-square-o': customer.primary}">--}}
                {{--</i> Primary--}}
            {{--</button>--}}
        </div>
    </div>
</div>

<div class="form-group">
    <label class="control-label">Department
        <small class="text-muted">(Optional)</small>
    </label>
    <input name="department" type="text" class="form-control" placeholder="Department"
           v-model="customer.pivot.department">
</div>

<div class="form-group">
    <label class="control-label">Position
        <small class="text-muted">(Optional)</small>
    </label>
    <input name="position" type="text" class="form-control" placeholder="Position"
           v-model="customer.pivot.position">
</div>

