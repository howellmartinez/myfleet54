@extends('layouts.app')

@section('content')

    <a href="{!! action('PersonController@index') !!}">Back to Persons</a>

    <br><br>

    <h2>Create Person</h2>

    <br>

    <div id="person" v-cloak>
        <form @submit.prevent="store" @keydown="form.errors.clear($event.target.name)">
            @include('persons._form')
        </form>
    </div>
@endsection