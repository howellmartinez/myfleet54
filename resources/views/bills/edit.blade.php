@extends('layouts.app')

@section('content')

    <div v-cloak id="bill">

        <a href="{!! action('BillController@index') !!}">Back to Bills</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="bill.isBusy" :is-deleting="bill.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Bill</h2>

        <br>

        <form @submit.prevent="update" v-on:keydown="bill.errors.clear($event.target.name)">
            @include('bills._form')
            <timestamp :name="bill.updated_by" :time="bill.updated_at"></timestamp>
        </form>

    </div>

@endsection