<div class="row">
    <div class="form-group">
        <label class="control-label form-control-feedback">Ref No.</label>
        <input type="text" class="form-control" v-model="bill.ref_no">
    </div>

    <div class="form-group">
        <label class="control-label form-control-feedback">Bill Date</label>
        <input type="date" class="form-control" v-model="bill.date">
    </div>
</div>


<div class="card">
    <div class="card-header">Trip Details</div>
    <div class="card-block">
        <div class="row">
            <div class="col-6" style="border-right: #eeeeee solid 1px">
                <div class="form-group row" :class="{'has-danger': bill.errors.has('trip_id')}">
                    <label class="col-4 col-form-label form-control-feedback">Trip Ticket No.</label>
                    <div class="col-8">
                        <select class="form-control" name="trip_id"
                                v-model="selectedTrip" v-on:change="bill.errors.clear('trip_id')">
                            <option v-for="trip in trips" :value="trip">@{{ trip.ticket_no }}</option>
                        </select>
                        <small class="form-text form-control-feedback" v-if="bill.errors.has('trip_id')">
                            @{{ bill.errors.get('trip_id') }}
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Trip Date</label>
                    <div class="col-8">
                        <input type="date" class="form-control" readonly
                               v-model="selectedTrip.date">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Rental Type</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Rental Type" readonly
                               v-model="selectedTrip.type">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Vehicle Type</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Vehicle Type" readonly
                               :value="this.car ? this.car.type : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Passenger</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Passenger" readonly
                               :value="this.passenger ? this.passenger.full_name : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Contact No.</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Contact No." readonly
                               v-model="selectedTrip.passenger_contact">
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-4 col-form-label">Time Start</label>
                            <div class="col-8">
                                <input type="text" class="form-control" placeholder=""
                                       v-model="selectedTrip.start">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-4 col-form-label">Time End</label>
                            <div class="col-8">
                                <input type="text" class="form-control" placeholder=""
                                       v-model="selectedTrip.end">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Pick-up Location</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Pick-up Location" readonly
                               v-model="selectedTrip.pickup_place">
                    </div>
                </div>
            </div>

            <div class="col-6">

                <div class="form-group row">
                    <label class="col-4 col-form-label">CRAF No.</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="CRAF No." readonly
                               :value="reservation ? reservation.ref_no : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Date Reserved</label>
                    <div class="col-8">
                        <input type="date" class="form-control" readonly
                               :value="reservation ? reservation.date : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Business Unit</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Business Unit" readonly
                               :value="customer ? customer.name : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Reserved By</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Reserved By" readonly
                               :value="requestor ? requestor.full_name : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Approved By</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Approved By" readonly
                               :value="authorizer ? authorizer.full_name : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Purpose</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Purpose" readonly
                               :value="reservation ? reservation.purpose : ''">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Charging Account</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Charging Account" readonly
                               :value="reservation ? reservation.charging_loc : ''">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="card">
    <div class="card-header">
        Summary of Charges
    </div>

    <div class="card-block">

        <div class="row">
            <div class="col-6" style="border-right: #eeeeee solid 1px">
                <div class="row">
                    <div class="col-12"><b>Service Rendered:</b></div>
                </div>

                <br>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Regular Fare:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.regular_fare">
                        {{--<input type="text" class="form-control text-right" placeholder="0.00" v-model="bill.test">--}}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Excess Hours:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.excess_hours_charge">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Driver OT:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.driver_ot_charge">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Total:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01" readonly
                               v-model.number="bill.vatable_total">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Add VAT:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.vat">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Total Amount Due:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01" readonly
                               v-model.number="bill.service_total">
                    </div>
                </div>
            </div>


            <div class="col-6">
                <div class="row">
                    <div class="col-12"><b>Reimbursement Charges:</b></div>
                </div>

                <br>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Fuel:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.fuel_charge">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Meals:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.meals">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Toll:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.tolls">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Parking:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.parking">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Lodging:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.lodging">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Others:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01"
                               v-model.number="bill.others">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-4 col-form-label">Total Reimbursement:</label>
                    <div class="col-8">
                        <input type="number" class="form-control text-right" placeholder="0.00" step="0.01" readonly
                               v-model.number="bill.reimbursement_total">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">

    <div class="col-6">
        <div class="card">
            <div class="card-block">
                Total: (Service Rendered + Reimbursement Charges)

                <br><br>

                <h1 class="text-center">@{{ bill.total }}</h1>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="card">
            <div class="card-block">
                <div class="form-group" :class="{'has-danger': bill.errors.has('approved_by')}">
                    <label class="control-label form-control-feedback">Approved By</label>
                    <input type="text" class="form-control" placeholder="Approved By" name="approved_by"
                           v-model="bill.approved_by">
                    <small class="form-text form-control-feedback" v-if="bill.errors.has('approved_by')">
                        @{{ bill.errors.get('approved_by') }}
                    </small>
                </div>
                {{--Approved By:--}}

                {{--<br><br>--}}

                {{--<div class="text-center" style="border-top: #eeeeee solid 1px">--}}
                {{--<b>JESS CANLAPAN</b>--}}
                {{--<br>--}}
                {{--(Signature over printed name)--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>

<hr>

<save-button :is-busy="bill.isBusy" :is-saving="bill.isSaving"></save-button>

@push('scripts')
<script src="/js/bill.js"></script>
@endpush