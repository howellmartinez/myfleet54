@extends('layouts.app')

@section('content')

    <div v-cloak id="bill">

        <a href="{!! action('BillController@index') !!}">Back to Bills</a>

        <br><br>

        <h2>Create Bill</h2>

        <br>

        <form @submit.prevent="store" v-on:keydown="bill.errors.clear($event.target.name)">
            @include('bills._form')
        </form>
    </div>

@endsection