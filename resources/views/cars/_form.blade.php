{!! csrf_field() !!}

<div class="row">
    <div class="col-md-6">
        <div class="card">

            <div class="card-header">Car Info</div>

            <div class="card-block">

                <div class="form-group">
                    <label class="control-label">Type / Class</label>
                    <select name="class" class="form-control" v-model="car.class">
                        <optgroup v-for="classification in classifications" :label="classification.type">
                            <option v-for="carClass in classification.classes" :value="carClass">
                                @{{ classification.type }} - @{{ carClass }}
                            </option>
                        </optgroup>
                    </select>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('make')}">
                    <label class="control-label form-control-feedback">Make</label>
                    <input name="make" type="text" class="form-control" placeholder="Make"
                           v-model="car.make">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('make')">
                        @{{ car.errors.get('make') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('model')}">
                    <label class="control-label form-control-feedback">Model</label>
                    <input name="model" type="text" class="form-control" placeholder="Model"
                           v-model="car.model">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('model')">
                        @{{ car.errors.get('model') }}
                    </small>
                </div>

                <div class="form-group">
                    <label class="control-label">Fuel System</label>
                    <select name="fuel" class="form-control"
                            v-model="car.fuel">
                        <option>Gas</option>
                        <option>Diesel</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Transmission</label>
                    <select name="tx" class="form-control"
                            v-model="car.tx">
                        <option>MT</option>
                        <option>AT</option>
                    </select>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('year')}">
                    <label class="control-label form-control-feedback">Year</label>
                    <input name="year" type="text" class="form-control" placeholder="Year"
                           v-model="car.year">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('year')">
                        @{{ car.errors.get('year') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('color')}">
                    <label class="control-label form-control-feedback">Color</label>
                    <input name="color" type="text" class="form-control" placeholder="Color"
                           v-model="car.color">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('color')">
                        @{{ car.errors.get('color') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('max_cap')}">
                    <label class="control-label form-control-feedback">Max. Capacity</label>
                    <input name="max_cap" type="number" class="form-control" step="1" min="0" placeholder="0"
                           v-model="car.max_cap">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('max_cap')">
                        @{{ car.errors.get('max_cap') }}
                    </small>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">

            <div class="card-header">Registration Info</div>

            <div class="card-block">
                <div class="form-group">
                    <label class="control-label">Plate No. <small class="text-muted">(Optional)</small></label>
                    <input name="plate" type="text" class="form-control" placeholder="Plate No."
                           v-model="car.plate">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('plate')">
                        @{{ car.errors.get('plate') }}
                    </small>
                </div>

                <div class="form-group">
                    <label class="control-label">Conduction No.</label>
                    <input name="conduction_no" type="text" class="form-control" placeholder="Conduction No."
                           v-model="car.conduction_no">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('conduction_no')">
                        @{{ car.errors.get('conduction_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('engine_no')}">
                    <label class="control-label form-control-feedback">Engine No.</label>
                    <input name="engine_no" type="text" class="form-control" placeholder="Engine No."
                           v-model="car.engine_no">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('engine_no')">
                        @{{ car.errors.get('engine_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('serial_no')}">
                    <label class="control-label form-control-feedback">Serial No.</label>
                    <input name="serial_no" type="text" class="form-control" placeholder="Serial No."
                           v-model="car.serial_no">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('serial_no')">
                        @{{ car.errors.get('serial_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('or_no')}">
                    <label class="control-label form-control-feedback">OR No.</label>
                    <input name="or_no" type="text" class="form-control" placeholder="OR No."
                           v-model="car.or_no">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('or_no')">
                        @{{ car.errors.get('or_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('cr_no')}">
                    <label class="control-label form-control-feedback">CR No.</label>
                    <input name="cr_no" type="text" class="form-control" placeholder="CR No."
                           v-model="car.cr_no">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('cr_no')">
                        @{{ car.errors.get('cr_no') }}
                    </small>
                </div>

                <div class="form-group" :class="{'has-danger': car.errors.has('registered_owner')}">
                    <label class="control-label form-control-feedback">Registered Owner</label>
                    <input name="registered_owner" type="text" class="form-control" placeholder="Registered Owner"
                           v-model="car.registered_owner">
                    <small class="form-text form-control-feedback" v-if="car.errors.has('registered_owner')">
                        @{{ car.errors.get('registered_owner') }}
                    </small>
                </div>

                <div class="form-group">
                    <label class="control-label">Status</label>
                    <select name="status" class="form-control"
                            v-model="car.status">
                        <option>Active</option>
                        <option>Inactive</option>
                        <option>Sold</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<save-button :is-busy="car.isBusy" :is-saving="car.isSaving"></save-button>

@push('scripts')
<script src="/js/car.js"></script>
@endpush