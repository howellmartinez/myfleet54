@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>
        <a href="{!! action('CarController@create') !!}">Register Car</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Cars</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Plate</th>
                    <th>Type</th>
                    <th>Class</th>
                    <th>Make</th>
                    <th>Model</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.plate }}</td>
                    <td>@{{ item.type }}</td>
                    <td>@{{ item.class }}</td>
                    <td>@{{ item.make }}</td>
                    <td>@{{ item.model }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>
@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush