@extends('layouts.app')

@section('content')

    <div id="car" v-cloak>
        <a href="{!! action('CarController@index') !!}">Back to Cars</a>

        <br><br>

        <form @submit.prevent="update" v-on:keydown="car.errors.clear($event.target.name)">

        <h2>Edit Car</h2>

        <br>

        <form @submit.prevent="update" v-on:keydown="car.errors.clear($event.target.name)">
            @include('cars._form')
            <timestamp :name="car.updated_by" :time="car.updated_at"></timestamp>
        </form>
    </div>
@stop