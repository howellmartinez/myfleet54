@extends('layouts.app')

@section('content')

    <div id="car" v-cloak>
        <a href="{!! action('CarController@index') !!}">Back to Cars</a>

        <br><br>

        <h2>Register Car</h2>

        <br>

        <form @submit.prevent="store" v-on:keydown="car.errors.clear($event.target.name)">
            @include('cars._form')
        </form>
    </div>
@stop