{!! csrf_field() !!}

<div class="row">
    <div class="col-md-8">
        <div class="form-group" :class="{'has-danger': form.errors.has('name')}">
            <label class="control-label form-control-feedback">Name</label>
            <input name="name" type="text" class="form-control" placeholder="Name"
                   v-model="form.name">
            <small class="form-text form-control-feedback" v-if="form.errors.has('name')">
                @{{ form.errors.get('name') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('hours')}">
            <label class="control-label form-control-feedback">Hours</label>
            <input name="hours" type="text" class="form-control" placeholder="Hours"
                   v-model="form.hours">
            <small class="form-text form-control-feedback" v-if="form.errors.has('hours')">
                @{{ form.errors.get('hours') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('rate')}">
            <label class="control-label form-control-feedback">Rate</label>
            <input name="rate" type="text" class="form-control" placeholder="Rate"
                   v-model="form.rate">
            <small class="form-text form-control-feedback" v-if="form.errors.has('rate')">
                @{{ form.errors.get('rate') }}
            </small>
        </div>

    </div>
</div>

<hr>

<save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>

@push('scripts')
<script src="/js/trip-type.js"></script>
@endpush