@extends('layouts.app')

@section('content')

    <a href="{!! action('TripTypeController@index') !!}">Back to Trip Types</a>

    <br><br>

    <h2>Create Trip Type</h2>

    <br>

    <div id="trip-type" v-cloak>
        <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)">
            @include('trip_types._form')
        </form>
    </div>
@endsection