@extends('layouts.app')

@section('content')

    <div id="trip-type" v-cloak>
        <a href="{!! action('TripTypeController@index') !!}">Back to Trip Types</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Trip Type</h2>

        <br>

        <form @submit.prevent="update" v-on:keydown="form.errors.clear($event.target.name)">
            @include('trip_types._form')
            <timestamp :name="form.updated_by" :time="form.updated_at"></timestamp>
        </form>
    </div>
@endsection