@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

        <a href="{!! action('ReceiptController@create') !!}">Create Receipt</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Receipts</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Ref No.</th>
                    <th>Date</th>
                    <th>Customer</th>
                    <th class="text-right">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.ref_no }}</td>
                    <td>@{{ item.date }}</td>
                    <td>@{{ item.customer.name }}</td>
                    <td class="text-right">@{{ item.total }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>
@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush