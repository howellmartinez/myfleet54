@extends('layouts.app')

@section('content')

    <div id="receipt" v-cloak>
        <a href="{!! action('ReceiptController@index') !!}">Back to Receipts</a>

        <br><br>

        <div class="pull-right">
            <delete-button :is-busy="receipt.isBusy" :is-deleting="receipt.isDeleting" @destroy="destroy">
        </div>

        <h2>Edit Receipt</h2>

        <br>

        <form @submit.prevent="update">
            @include('receipts._form')
        </form>
    </div>
@stop