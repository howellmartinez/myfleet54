@extends('layouts.app')

@section('content')

    <a href="{!! action('ReceiptController@index') !!}">Back to Receipts</a>

    <br><br>

    <h2>Create Receipt</h2>

    <br>

    <div id="receipt" v-cloak>
        <form @submit.prevent="store">
            @include('receipts._form')
        </form>
    </div>
@stop