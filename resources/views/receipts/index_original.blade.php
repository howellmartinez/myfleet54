@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

        <a href="{!! action('ReceiptController@create') !!}">Create Receipt</a>

        <br><br>

        <filters :conditions="conditions"
                 :filterable-fields="filterableFields"
                 v-on:add="addCondition"
                 v-on:filter="filter"
                 v-on:clear="clear">
        </filters>

        <br>

        <h2>Receipts</h2>

        <br>


        <table class="table">
            <thead>
            <tr>
                <th>Ref No.</th>
                <th>Date</th>
                <th>Customer</th>
                <th class="text-right">Total</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in items" v-if="!isLoading">
                <td><a :href="'/app/receipts/' + item.id + '/edit'">@{{ item.ref_no }}</a></td>
                <td>@{{ item.date }}</td>
                <td>@{{ item.customer.name }}</td>
                <td class="text-right">@{{ item.total }}</td>
            </tr>
            </tbody>
        </table>

        <p class="text-center" v-if="isLoading">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...
        </p>

        <p class="text-center" v-if="!isLoading && noItems">
            No items.
        </p>

        <hr>

        <pagination-links
                :status="paginationStatus"
                :prev-disabled="isFirstPage || isLoading"
                :next-disabled="isLastPage || isLoading"
                v-on:next-page="nextPage"
                v-on:prev-page="prevPage">
        </pagination-links>

    </div>

@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush