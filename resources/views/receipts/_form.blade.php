{!! csrf_field() !!}

<div class="row">
    <div class="col-2">
        <div class="form-group">
            <label class="control-label">Ref No.</label>
            <input name="ref_no" type="text" class="form-control" placeholder="Ref No."
                   v-model="receipt.ref_no">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-8">
        <div class="form-group">
            <label class="control-label">Customer</label>
            <select class="form-control" v-model="selectedCustomer">
                <option v-for="customer in customers" :value="customer">
                    @{{ customer.name }}
                </option>
            </select>
        </div>
    </div>

    <div class="offset-1 col-3">
        <div class="form-group">
            <label class="control-label">Date</label>
            <input name="date" type="date" class="form-control" v-model="receipt.date">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-header">Payments</div>

            <div class="card-block">
                <table class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Ref No.</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="payment in customerPayments">
                        <td><input type="checkbox" v-model="receipt.payments" :value="payment.id"></td>
                        <td>@{{ payment.date }}</td>
                        <td>@{{ payment.ref_no }}</td>
                        <td>@{{ payment.amount }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-block">
                <h2>@{{ total.toFixed(2) }}</h2>
            </div>
        </div>
    </div>
</div>

<hr>

<button type="submit" class="btn btn-primary">
    <i class="fa fa-save"></i> Save
</button>

@push('scripts')
<script src="/js/receipt.js"></script>
@endpush