<footer class="footer">
    <div class="container">
        <p class="text-muted float-right">
            Powered by
            <a href="http://www.cometonesolutions.com">CometOne Solutions</a>
        </p>
    </div>
</footer>