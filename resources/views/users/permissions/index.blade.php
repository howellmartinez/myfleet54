@extends('layouts.app')

@section('content')

    <div id="permissions" v-cloak>

        <a href="{!! action('UserController@create') !!}">Create User</a>

        <form @submit.prevent="save">


            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">User</th>
                    <template v-for="(permissions, group) in permissionGroups">
                        <th :colspan="permissions.length">@{{ group }}</th>
                    </template>
                </tr>
                <tr>
                    <template v-for="(permissions, group) in permissionGroups">
                        <th v-for="permission in permissions">
                            <i class="fa" :class="icons[permission.name.split(' ')[0]]"></i>
                        </th>
                    </template>
                </tr>
                </thead>
                <tbody>
                <tr v-for="user in users">
                    <td>@{{ user.name }}</td>
                    <template v-for="(permissions, group) in permissionGroups">
                        <td v-for="permission in permissions">
                            <input type="checkbox" :value="permission.name" v-model="user.permission_names">
                        </td>
                    </template>
                </tr>
                </tbody>
            </table>

            <save-button :is-busy="false" :is-saving="isSaving"></save-button>
        </form>

    </div>
@stop

@push('scripts')
<script src="/js/permissions.js"></script>
@endpush