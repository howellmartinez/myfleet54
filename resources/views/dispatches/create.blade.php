@extends('layouts.app')

@section('content')

    <a href="{!! action('DispatchController@index') !!}">Back to Dispatches</a>

    <br><br>

    <h2>Create Dispatch</h2>

    <br>

    @include('dispatches._form')


@endsection