<table class="table table-condensed">
    <thead>
    <tr>
        <th></th>
        <th>PO #</th>
        <th>Reservation Date</th>
        <th>Trip Date</th>
        <th>Company</th>
        <th>Vehicle Req.</th>
        <th>Pickup Place</th>
        <th>Destination</th>
    </tr>
    </thead>
    <tbody>
        <tr v-for="reservation in reservations">
            <td><input type="checkbox" v-model="reservation.checked"></td>
            <td>@{{ reservation.serial }}</td>
            <td>@{{ reservation.date }}</td>
            <td>@{{ reservation.trip_date }}</td>
            <td>@{{ reservation.customer.name }}</td>
            <td>@{{ reservation.vehicle }}</td>
            <td>@{{ reservation.pickup_place }}</td>
            <td>@{{ reservation.destination }}</td>
        </tr>
    </tbody>
</table>

<hr>