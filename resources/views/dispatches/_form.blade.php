<div id="dispatch">

    {!! csrf_field() !!}

    @include('dispatches._reservations')

    <template v-for="reservation in checkedReservations">
    <div class="card">
        <div class="card-header">@{{ reservation.serial }} Trip</div>
        <div class="card-block">
                {{--<dl class="row">--}}
                    {{--<dt class="col-sm-2">Customer</dt>--}}
                    {{--<dd class="col-sm-3">@{{ reservation.customer.name }}</dd>--}}

                    {{--<dt class="offset-3 col-sm-2">Reservation Date</dt>--}}
                    {{--<dd class="col-sm-2">@{{ reservation.date }}</dd>--}}

                    {{--<dt class="col-sm-2">Department</dt>--}}
                    {{--<dd class="col-sm-3">@{{ reservation.department }}</dd>--}}

                    {{--<dt class="offset-3 col-sm-2">Charging Loc.</dt>--}}
                    {{--<dd class="col-sm-2">@{{ reservation.charging_loc }}</dd>--}}

                    {{--<dt class="col-sm-2">Requestor</dt>--}}
                    {{--<dd class="col-sm-3">@{{ reservation.requestor.full_name }}</dd>--}}

                    {{--<dt class="col-sm-2"><i class="fa fa-phone"></i></dt>--}}
                    {{--<dd class="col-sm-5">@{{ reservation.requestor.phones }}</dd>--}}

                    {{--<dt class="col-sm-2">Authorizer</dt>--}}
                    {{--<dd class="col-sm-3">@{{ reservation.authorizer.full_name }}</dd>--}}

                    {{--<dt class="col-sm-2"><i class="fa fa-phone"></i></dt>--}}
                    {{--<dd class="col-sm-5">@{{ reservation.authorizer.phones }}</dd>--}}

                    {{--<dt class="col-sm-2">Purpose</dt>--}}
                    {{--<dd class="col-sm-10">@{{ reservation.purpose }}</dd>--}}

                    {{--<dt class="col-sm-2">PO Doc.</dt>--}}
                    {{--<dd class="col-sm-6">@{{ reservation.po_document_path }}</dd>--}}

                    {{--<dt class="col-sm-2">Ref No.</dt>--}}
                    {{--<dd class="col-sm-2">@{{ reservation.po_document_path }}</dd>--}}
                {{--</dl>--}}

                {{--<hr>--}}

                {{--<div class="card">--}}
                    {{--<div class="card-header">Dispatch</div>--}}
                    {{--<div class="card-block">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Car</label>--}}
                                    {{--<select class="form-control">--}}
                                        {{--@foreach($cars as $car)--}}
                                            {{--<option value="{{ $car->id }}">{{ $car->id }}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Driver</label>--}}
                                    {{--<select class="form-control">--}}
                                        {{--@foreach($drivers as $driver)--}}
                                            {{--<option>{{ $driver->last_name }}, {{ $driver->first_name }}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}

        </div>
    </div>
    </template>



    {{--<br>--}}

    {{--<div class="row">--}}

    {{--Trip Info--}}
    {{--<div class="col-md-6">--}}
    {{--<div class="card">--}}
    {{--<div class="card-header">Trip Info</div>--}}
    {{--<div class="card-block">--}}
    {{--<div class="form-group">--}}
    {{--<label class="control-label">Ticket No.</label>--}}
    {{--<input type="text" name="ticket_no" class="form-control" placeholder="Ticket No."--}}
    {{--value="{{ $trip->ticket_no or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Trip Date--}}
    {{--<small>({{ $reservation->trip_date }})</small>--}}
    {{--</label>--}}
    {{--<input type="date" name="date" class="form-control" value="{{ $trip->date or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Trip Type--}}
    {{--<small>({{ $reservation->type }})</small>--}}
    {{--</label>--}}
    {{--<select name="type" class="form-control">--}}
    {{--@foreach(config('myfleet.trip_types') as $trip_type)--}}
    {{--<option>{{ $trip_type }}</option>--}}
    {{--@endforeach--}}
    {{--</select>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Trip Location--}}
    {{--<small>({{ $reservation->location }})</small>--}}
    {{--</label>--}}
    {{--<select name="location" class="form-control">--}}
    {{--@foreach(config('myfleet.trip_locations') as $trip_location)--}}
    {{--<option>{{ $trip_location }}</option>--}}
    {{--@endforeach--}}
    {{--</select>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Vehicle--}}
    {{--<small>({{ $reservation->vehicle }})</small>--}}
    {{--</label>--}}
    {{--<select class="form-control">--}}
    {{--@foreach($cars as $car)--}}
    {{--<option value="{{ $car->id }}">{{ $car->id }}</option>--}}
    {{--@endforeach--}}
    {{--</select>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Pickup Place--}}
    {{--<small>({{ $reservation->pickup }})</small>--}}
    {{--</label>--}}
    {{--<input name="pickup" type="text" class="form-control" placeholder="Pickup Place"--}}
    {{--value="{{ $trip->pickup or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Destination--}}
    {{--<small>({{ $reservation->destination }})</small>--}}
    {{--</label>--}}
    {{--<input name="destination" type="text" class="form-control" placeholder="Destination"--}}
    {{--value="{{ $trip->destination or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Date/Time Start--}}
    {{--<small>({{ $reservation->trip_start }})</small>--}}
    {{--</label>--}}
    {{--<input name="trip_start" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->trip_start or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">--}}
    {{--Date/Time End--}}
    {{--<small>({{ $reservation->trip_end }})</small>--}}
    {{--</label>--}}
    {{--<input name="trip_end" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->trip_end or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Odometer Start</label>--}}
    {{--<input name="odo_end" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->odo_start or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Odometer End</label>--}}
    {{--<input name="odo_end" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->odo_end or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Fuel Start</label>--}}
    {{--<input name="fuel_start" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->fuel_start or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Fuel End</label>--}}
    {{--<input name="fuel_end" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->fuel_end or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Garage Start</label>--}}
    {{--<input name="garage_start" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->garage_start or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Garage End</label>--}}
    {{--<input name="garage_end" type="datetime-local" class="form-control"--}}
    {{--value="{{ $trip->garage_end or '' }}">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label">Trip Notes</label>--}}
    {{--<textarea class="form-control"></textarea>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}





    {{--</div>--}}

    <hr>

    <button class="btn btn-primary" type="submit">
        <i class="fa fa-save"></i> Save
    </button>

</div>

@push('scripts')
<script src="/js/dispatch.js"></script>
@endpush