@extends('layouts.app')

@section('content')

    <a href="{!! action('DispatchController@create') !!}">Create Dispatch</a>

    <br><br>

    <h2>Create Dispatch</h2>

    <table class="table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Driver</th>
            <th>Car</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
            @foreach($dispatches as $dispatch)
            <tr>
                <td>{{ $dispatch->date }}</td>
                <td>{{ $dispatch->driver->full_name }}</td>
                <td>{{ $dispatch->car->id }}</td>
                <td>{{ $dispatch->status }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop