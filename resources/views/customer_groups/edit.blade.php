@extends('layouts.app')

@section('content')

    <div id="customer-group" v-cloak>
        <a href="{!! action('TripTypeController@index') !!}">Back to Customer Groups</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="customer_group.isBusy" :is-deleting="customer_group.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Customer Group</h2>

        <br>

        <form @submit.prevent="update" v-on:keydown="customer_group.errors.clear($event.target.name)">
            @include('customer_groups._form')
            <timestamp :name="customer_group.updated_by" :time="customer_group.updated_at"></timestamp>
        </form>
    </div>
@endsection