@extends('layouts.app')

@section('content')

    <a href="{!! action('CustomerGroupController@index') !!}">Back to Customer Groups</a>

    <br><br>

    <h2>Create Customer Group</h2>

    <br>

    <div id="customer-group" v-cloak>
        <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)">
            @include('customer_groups._form')
        </form>
    </div>
@endsection