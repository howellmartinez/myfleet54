{!! csrf_field() !!}

<div class="row">
    <div class="col-md-8">
        <div class="form-group" :class="{'has-danger': form.errors.has('name')}">
            <label class="control-label form-control-feedback">Name</label>
            <input name="name" type="text" class="form-control" placeholder="Name"
                   v-model="form.name">
            <small class="form-text form-control-feedback" v-if="form.errors.has('name')">
                @{{ form.errors.get('name') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('code')}">
            <label class="control-label form-control-feedback">Code</label>
            <input name="code" type="text" class="form-control" placeholder="Code"
                   v-model="form.code">
            <small class="form-text form-control-feedback" v-if="form.errors.has('code')">
                @{{ form.errors.get('codes') }}
            </small>
        </div>

    </div>
</div>

<hr>

<save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>

@push('scripts')
<script src="/js/customer-group.js"></script>
@endpush