@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>
        <a href="{!! action('CustomerGroupController@create') !!}">Create Customer Group</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Customer Groups</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.name }}</td>
                    <td>@{{ item.code }}</td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>
@endsection

@push('scripts')
<script src="/js/index.js"></script>
@endpush