@extends('layouts.app')

@section('content')

    <a href="{!! action('TransmittalController@index') !!}">Back to Transmittals</a>

    <br><br>

    <h2>Create Transmittal</h2>

    <br>

    <div id="transmittal" v-cloak>
        <form @submit.prevent="store">
            @include('transmittals._form')
        </form>
    </div>
@endsection