@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

        <a href="{!! action('TransmittalController@create') !!}">Create Transmittal</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">
            <slot>
                <div slot="header">Transmittals</div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ref No.</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th class="text-right">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items" v-if="!isLoading">
                        <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                        <td>@{{ item.ref_no }}</td>
                        <td>@{{ item.date }}</td>
                        <td>@{{ item.customer.name }}</td>
                        <td class="text-right">@{{ item.amount }}</td>
                    </tr>
                    </tbody>
                </table>
            </slot>
        </index>
    </div>


@endsection

@push('scripts')
<script src="/js/index.js"></script>
@endpush