{!! csrf_field() !!}

<div class="row">
    <div class="offset-8 col-md-4">
        <div class="form-group">
            <label class="control-label">Date</label>
            <input type="date" class="form-control" v-model="transmittal.date">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="control-label">Customer</label>
            <select class="form-control" v-model="selectedCustomer">
                <option v-for="customer in customers" :value="customer">
                    @{{ customer.name }}
                </option>
            </select>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Ref No.</label>
            <input type="text" class="form-control" placeholder="Ref No."
                   v-model="transmittal.ref_no">
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">Transmittal SOAs</div>
    <div class="card-block">
        <table class="table small table-condensed">
            <thead>
            <tr>
                <th></th>
                <th>Invoice No.</th>
                <th>Charged To</th>
                <th>SOA No.</th>
                <th class="text-right">Vatable Sales</th>
                <th class="text-right">VAT</th>
                <th class="text-right">Total Service Rendered</th>
                <th class="text-right">Total Reimbursement</th>
                <th class="text-right">Total Amount</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="statement in customerStatements">
                <td><input type="checkbox" v-model="transmittal.statements" :value="statement.id"></td>
                <td>@{{ statement.invoice_no }}</td>
                <td>@{{ statement.id }}</td>
                <td>@{{ statement.ref_no }}</td>
                <td class="text-right">@{{ statement.vatable_total }}</td>
                <td class="text-right">@{{ statement.vat }}</td>
                <td class="text-right">@{{ statement.service_total }}</td>
                <td class="text-right">@{{ statement.reimbursement_total }}</td>
                <td class="text-right">@{{ statement.total }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<br>

<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-block">
                Total:
                <br>
                <h2 class="text-right">@{{ total.toFixed(2) }}</h2>
            </div>
        </div>
    </div>
</div>

<hr>

<save-button :is-busy="transmittal.isBusy" :is-saving="transmittal.isSaving"></save-button>

@push('scripts')
<script src="/js/transmittal.js"></script>
@endpush