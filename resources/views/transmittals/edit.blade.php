@extends('layouts.app')

@section('content')

    <a href="{!! action('TransmittalController@index') !!}">Back to Transmittals</a>

    <br><br>

    <div id="transmittal" v-cloak>

        <div class="pull-right">
            <delete-button :is-busy="transmittal.isBusy" :is-deleting="transmittal.isDeleting" @destroy="destroy">
        </div>

        <h2>Edit Transmittal</h2>

        <br>

        <form @submit.prevent="update">
            @include('transmittals._form')
        </form>
    </div>

@endsection