@extends('layouts.app')

@section('content')

    <a href="{!! action('CustomerController@index') !!}">Back to Customers</a>

    <br><br>


    <div id="customer" v-cloak>

        <div class="float-right">
            <delete-button :is-busy="form.isBusy" :is-deleting="form.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Customer</h2>

        <br>

        <form v-on:submit.prevent="update" v-on:keydown="form.errors.clear($event.target.name)">
            @include('customers._form')
            <timestamp :name="form.updated_by" :time="form.updated_at"></timestamp>
        </form>
    </div>
@stop