@extends('layouts.app')

@section('content')

    <div id="index" v-cloak>

        <a href="{!! action('CustomerController@create') !!}">Create Customer</a>

        <br><br>

        <index :filterable-fields="filterableFields"
               :base-url="baseUrl"
               v-on:update-loading="(val) => isLoading = val"
               v-on:update-items="(val) => items = val">

            <div slot="header">Customers</div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Group</th>
                    <th class="text-center">VAT</th>
                    <th>MFI Code</th>
                    <th>Person</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in items" v-if="!isLoading">
                    <td><a :href="item.edit_url">@{{ item.serial }}</a></td>
                    <td>@{{ item.name }}</td>
                    <td>@{{ item.customer_group.code }}</td>
                    <td class="text-center">
                        <span v-if="item.vat"><i class="text-success fa fa-check-circle fa-lg"></i></span>
                        <span v-if="!item.vat"><i class="text-muted fa fa-minus-circle fa-lg"></i></span>
                    </td>
                    <td>@{{ item.mfi_code }}</td>
                    <td>
                        <span v-if="item.primary_person.length > 0">
                            <a :href="'/app/persons/' + item.primary_person[0].id + '/edit'">
                                @{{ item.primary_person[0].full_name }}
                            </a>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </index>
    </div>

@stop

@push('scripts')
<script src="/js/index.js"></script>
@endpush