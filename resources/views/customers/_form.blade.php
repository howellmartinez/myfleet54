{!! csrf_field() !!}

<div class="row">
    <div class="col-md-5">
        <div class="form-group" :class="{'has-danger': form.errors.has('name')}">
            <label class="control-label form-control-feedback">Name</label>
            <input name="name" type="text" class="form-control" placeholder="Name" v-model="form.name">
            <small class="form-text form-control-feedback" v-if="form.errors.has('name')">
                @{{ form.errors.get('name') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('customer_group_id')}">
            <label class="control-label form-control-feedback">Group</label>
            <select name="group" class="form-control" v-model="form.customer_group_id">
                <option v-for="customerGroup in customerGroups" :value="customerGroup.id">@{{ customerGroup.code }} - @{{ customerGroup.name }}</option>
            </select>
            <small class="form-text form-control-feedback" v-if="form.errors.has('customer_group_id')">
                @{{ form.errors.get('customer_group_id') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('address')}">
            <label class="control-label form-control-feedback">Address <small class="text-muted">(Optional)</small></label>
            <textarea name="address" class="form-control" placeholder="Address" v-model="form.address">
            </textarea>
            <small class="form-text form-control-feedback" v-if="form.errors.has('address')">
                @{{ form.errors.get('address') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('tin')}">
            <label class="control-label form-control-feedback">TIN <small class="text-muted">(Optional)</small></label>
            <input name="tin" type="text" class="form-control" placeholder="TIN" v-model="form.tin">
            <small class="form-text form-control-feedback" v-if="form.errors.has('tin')">
                @{{ form.errors.get('tin') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('vat')}">
            <label class="control-label form-control-feedback">VATable</label>

            <br>

            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="vat" :value="true" v-model="form.vat"> VAT
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="vat" :value="false" v-model="form.vat"> Non-VAT
                </label>
            </div>

            <small class="form-text form-control-feedback" v-if="form.errors.has('vat')">
                @{{ form.errors.get('vat') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('code')}">
            <label class="control-label form-control-feedback">Client Company Code</label>
            <input name="code" type="text" class="form-control" placeholder="Client Company Code" v-model="form.code">
            <small class="form-text form-control-feedback" v-if="form.errors.has('code')">
                @{{ form.errors.get('code') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('mfi_code')}">
            <label class="control-label form-control-feedback">MFI Code <small class="text-muted">(Optional)</small></label>
            <input name="mfi_code" type="text" class="form-control" placeholder="MFI Code" v-model="form.mfi_code">
            <small class="form-text form-control-feedback" v-if="form.errors.has('mfi_code')">
                @{{ form.errors.get('mfi_code') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': form.errors.has('terms')}">
            <label class="control-label form-control-feedback">Terms</label>
            <input name="terms" type="number" step="1" class="form-control" min="0" placeholder="0" v-model.number="form.terms">
            <small class="form-text form-control-feedback" v-if="form.errors.has('terms')">
                @{{ form.errors.get('terms') }}
            </small>
        </div>

        <div class="form-group">
            <label class="control-label">Notes <small class="text-muted">(Optional)</small></label>
            <textarea class="form-control" placeholder="Notes" v-model="form.notes"></textarea>
        </div>

        {{--<div class="form-group" :class="{'has-danger': form.errors.has('')}">--}}
            {{--<label class="control-label form-control-feedback">Primary Contact Person</label>--}}
            {{--<input name="person" type="text" class="form-control">--}}
        {{--</div>--}}

        {{--<div class="form-group" :class="{'has-danger': form.errors.has('')}">--}}
            {{--<label class="control-label form-control-feedback">Primary Contact Phone</label>--}}
            {{--<input name="phone" type="text" class="form-control">--}}
        {{--</div>--}}
    </div>
</div>


<hr>

<save-button :is-busy="form.isBusy" :is-saving="form.isSaving"></save-button>

@push('scripts')
<script src="/js/customer.js"></script>
@endpush