@extends('layouts.app')

@section('content')

    <a href="{!! action('CustomerController@index') !!}">Back to Customers</a>

    <br><br>

    <h2>Create Customer</h2>

    <br>

    <div id="customer" v-cloak>
        <form @submit.prevent="store" v-on:keydown="form.errors.clear($event.target.name)">
            @include('customers._form')
        </form>
    </div>
@endsection