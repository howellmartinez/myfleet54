<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/app">
        <img src="{{ asset('images/logo.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        {{ config('app.name') }}
    </a>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-list-alt"></i> Master Lists
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/app/customer-groups">Customer Groups</a>
                    <a class="dropdown-item" href="/app/customers">Customers</a>
                    <a class="dropdown-item" href="/app/trip-types">Trip Types</a>
                    <a class="dropdown-item" href="/app/persons">Persons</a>
                    <a class="dropdown-item" href="/app/cars">Cars</a>
                    <a class="dropdown-item" href="/app/drivers">Drivers</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-road"></i> Dispatch
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/app/reservations">Reservations</a>
                    <a class="dropdown-item" href="/app/trips">Trips</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-money"></i> Billing
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/app/bills">Trip Bills</a>
                    <a class="dropdown-item" href="/app/statements">SOAs</a>
                    <a class="dropdown-item" href="/app/transmittals">Transmittals</a>
                    <a class="dropdown-item" href="/app/payments">Payments</a>
                    <a class="dropdown-item" href="/app/receipts">Receipts</a>
                </div>
            </li>

        </ul>
    </div>
</nav>