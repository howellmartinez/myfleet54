{!! csrf_field() !!}

<div class="row">
    <div class="col-md-8">
        <div class="form-group" :class="{'has-danger': driver.errors.has('last_name')}">
            <label class="control-label form-control-feedback">Last Name</label>
            <input name="last_name" type="text" class="form-control" placeholder="Last Name"
                   v-model="driver.last_name">
            <small class="form-text form-control-feedback" v-if="driver.errors.has('last_name')">
                @{{ driver.errors.get('last_name') }}
            </small>
        </div>

        <div class="form-group" :class="{'has-danger': driver.errors.has('first_name')}">
            <label class="control-label form-control-feedback">First Name</label>
            <input name="first_name" type="text" class="form-control" placeholder="First Name"
                   v-model="driver.first_name">
            <small class="form-text form-control-feedback" v-if="driver.errors.has('first_name')">
                @{{ driver.errors.get('first_name') }}
            </small>
        </div>

        <div class="form-group">
            <label class="control-label">Middle Name</label>
            <input name="middle_name" type="text" class="form-control" placeholder="Middle Name"
                   v-model="driver.middle_name">
        </div>

        <div class="form-group">
            <label class="control-label">Notes</label>
            <textarea class="form-control" placeholder="Notes" name="notes"
                      v-model="driver.notes"></textarea>
        </div>
    </div>
</div>

<hr>

<save-button :is-busy="driver.isBusy" :is-saving="driver.isSaving"></save-button>

@push('scripts')
<script src="/js/driver.js"></script>
@endpush