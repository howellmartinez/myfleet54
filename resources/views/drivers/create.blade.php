@extends('layouts.app')

@section('content')

    <a href="{!! action('DriverController@index') !!}">Back to Drivers</a>

    <br><br>

    <h2>Create Driver</h2>

    <br>

    <div id="driver" v-cloak>
        <form @submit.prevent="store" v-on:keydown="driver.errors.clear($event.target.name)">
            @include('drivers._form')
        </form>
    </div>
@endsection