@extends('layouts.app')

@section('content')

    <div id="driver" v-cloak>
        <a href="{!! action('DriverController@index') !!}">Back to Drivers</a>

        <br><br>

        <div class="float-right">
            <delete-button :is-busy="driver.isBusy" :is-deleting="driver.isDeleting" @destroy="destroy"></delete-button>
        </div>

        <h2>Edit Driver</h2>

        <br>

        <form @submit.prevent="update" v-on:keydown="driver.errors.clear($event.target.name)">
            @include('drivers._form')
            <timestamp :name="driver.updated_by" :time="driver.updated_at"></timestamp>
        </form>
    </div>
@endsection