import {Form} from './components/Form'

import FieldList from './components/FieldList'
import ErrorMessage from './components/ErrorMessage'
import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#customer-person',

    components: {
        FieldList, ErrorMessage, SaveButton, DeleteButton, Timestamp
    },

    data: {
        form: new Form({
            last_name: '',
            first_name: '',
            middle_name: '',
            nickname: '',
            position: '',
            customer_id: '',
            primary: false,
            notes: '',
            phones: [{label: '', value: ''}],
            emails: [{label: '', value: ''}],
            relationships: [{label: '', value: ''}],
            others: [{label: '', value: ''}],
        }),
        customers: customers,
    },

    methods: {

        store() {
            this.form.post('/app/persons')
                .then(response => {
                    this.$swal({
                        title: 'Person created!',
                        text: this.form.first_name + ' ' + this.form.last_name +  ' added to person database.',
                        type: 'success'
                    });
                    this.form.reset();
                }).catch(errors => console.log(errors));
        },

        update() {
            this.form.patch('/app/persons/' + this.form.id)
                .then(response => {
                    this.form.updated_by = response.updated_by;
                    this.form.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Person updated!',
                        text: 'Changes saved to person database.',
                        type: 'success'
                    });
                }).catch(errors => console.log(errors));
        },

        destroy() {
            this.form.delete('/app/persons/' + this.form.id)
                .then(data => {
                    this.$swal({
                        title: 'Person deleted!',
                        text: 'Person removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/persons');
                }).catch(errors => console.log(errors));
        },

        loadData(data) {
            console.log("Loading person data...");
            this.form = new Form({
                id: data.id,
                last_name: data.last_name,
                first_name: data.first_name,
                middle_name: data.middle_name,
                nickname: data.nickname,
                position: data.position,
                customer_id: data.customer_id,
                primary: data.primary,
                notes: data.notes,
                phones: JSON.parse(data['phones']),
                emails: JSON.parse(data['emails']),
                relationships: JSON.parse(data['emails']),
                others: JSON.parse(data['others']),
                updated_by: data.updated_by,
                updated_at: data.updated_at,
                created_at: data.created_at,
            });
        },

    },

    mounted() {
        console.log("Init person script...");
        if (person != null) {
            this.loadData(person);
        }
    }
});