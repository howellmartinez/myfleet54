import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#trip-type',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        form: new Form({
            name: '',
            hours: '',
            rate: '',
        }),
    },

    methods: {

        destroy() {
            this.form.delete('/app/api/trip-types/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Trip Type deleted!',
                        text: 'Trip Type removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/trip-types');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.form.post('/app/api/trip-types')
                .then(response => {
                    this.$swal({
                        title: 'Trip Type created!',
                        text: 'Trip Type added to database.',
                        type: 'success'
                    });
                    this.form.reset();
                })
                .catch(error => console.log(error));
        },

        update() {
            this.form.patch('/app/api/trip-types/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.form.updated_by = response.updated_by;
                    this.form.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Trip type updated!',
                        text: 'Changes saved to Trip Type database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.form = new Form({
                id: data.id,
                name: data.name,
                hours: data.hours,
                rate: data.rate,
                updated_by: data.updated_by,
            });
        },
    },

    mounted() {
        console.log("Init trip-type script...");
        if(tripType != null) {
            this.loadData(tripType);
        }
    }
});