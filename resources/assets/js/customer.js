import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#customer',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        form: new Form({
            name: '',
            code: '',
            group: '',
            mfi_code: '',
            tin: '',
            vat: true,
            terms: 0,
            address: '',
            notes: '',
            customer_group_id: '',
        }),

        customerGroups: customerGroups,
    },

    methods: {
        destroy() {
            this.form.delete('/app/customers/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Customer deleted!',
                        text: 'Customer removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/customers');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.form.post('/app/customers')
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Customer created!',
                        text: this.form.name + ' added to customer database.',
                        type: 'success'
                    });
                    this.form.reset();
                })
                .catch(error => console.log('Error:', error));
        },

        update() {
            this.form.patch('/app/customers/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.form.updated_by = response.updated_by;
                    this.form.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Customer updated!',
                        text: 'Changes saved to customer database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.form = new Form(data);
        },
    },

    mounted() {
        console.log("Init customer script...");
        if(customer != null) {
            this.loadData(customer);
        }
    }
});