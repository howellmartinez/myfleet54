import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'

import ErrorMessage from './components/ErrorMessage'

new Vue({
    el: '#statement',

    components: {
        SaveButton, DeleteButton, ErrorMessage
    },

    data: {

        statement: new Form({
            ref_no: '',
            customer_id: '',
            date: '',
            transmittal_id: '',
            invoice_no: '',
            total: 0.00,
            // TODO
            // updated_by: null,
            bills: [],
        }),

        selectedCustomer: {},

        customers: customers || [],

        trips: trips,
    },

    watch: {
        selectedCustomer(val) {
            this.statement.customer_id = val.id;
            if (val.id == this.statement.originalData.customer_id) {
                this.statement.bills = this.statement.originalData.bills;
            } else {
                this.statement.bills = [];
            }
        },

        total(val) {
            this.statement.total = val;
        },
    },

    computed: {
        hasCustomer() {
            return Object.keys(this.selectedCustomer).length > 0;
        },

        customerTrips() {
            if (!this.hasCustomer) return [];
            return this.trips.filter((trip) => trip.reservation.customer_id == this.selectedCustomer.id);
        },

        total() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.total), 0.00);
        },

        statementBills() {
            return this.trips
                .map(trip => trip.bill)
                .filter(bill => this.statement.bills.includes(bill.id));
        },

        vatableTotal() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.vatable_total), 0.00);
        },

        withholdingTax() {
            // TODO
        },

        vat() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.vat), 0.00);
        },

        serviceTotal() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.service_total), 0.00);
        },

        fuelCharge() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.fuel_charge), 0.00);
        },

        tolls() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.tolls), 0.00);
        },

        meals() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.meals), 0.00);
        },

        lodging() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.lodging), 0.00);
        },

        others() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.others), 0.00);
        },

        reimbursementTotal() {
            return this.statementBills.reduce((prev, curr) => prev + Number(curr.reimbursement_total), 0.00);
        },

    },

    methods: {

        check(method) {
            this.statement.check('/app/api/statements/' + this.statement.id + '/check')
                .then(response => {
                    if(response) {
                        console.log(response);
                        this[method]();
                    }
                }).catch(error => console.log(error));
        },

        destroy() {
            this.statement.delete('/app/statements/' + this.statement.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        store() {
            this.statement.post('/app/statements')
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        update() {
            // console.log('you are here', this.statement.id);
            this.statement.patch('/app/statements/' + this.statement.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.statement = new Form({
                id: data.id,
                ref_no: data.ref_no,
                customer_id: data.customer_id,
                date: data.date,
                transmittal_id: data.transmittal_id,
                invoice_no: data.invoice_no,
                total: data.total,
                updated_by: data.updated_by,
                bills: data.bills.map(bill => bill.id), // extract ids
            });

            this.selectedCustomer = this.customers.find(customer => data.customer_id == customer.id);
        },
    },

    mounted() {
        console.log("Init statement script...");
        if (statement != null) {
            this.loadData(statement);
        }
    }
});