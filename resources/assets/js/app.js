/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// require('jquery-mask-plugin');

import VueSweetAlert from 'vue-sweetalert'
Vue.use(VueSweetAlert);

require('select2');

$.fn.select2.defaults.set( "theme", "bootstrap" );

// Vue.directive('foo', function(el) {
//     $(el).select2();
// });

// Vue.directive('currency', function (el) {
//     $(el).mask('#,##0.00', {reverse: true});
    // $(el).change(() => {
    //     console.log($(el).val());
    // });
// });


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app',
//     mounted() {
//         console.log('App component mounted');
//     }
// });