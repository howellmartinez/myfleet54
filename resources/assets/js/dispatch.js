if (reservations != null) {
    console.log('you are here');
    reservations.map(reservation => {
        reservation.checked = false
    });
}

new Vue({
    el: '#dispatch',

    components: {
    },

    data: {
        reservations: reservations || [],
    },

    computed: {
        checkedReservations() {
            return this.reservations.filter(reservation => reservation.checked == true);
        }
    },

    methods: {

        destroy() {
            // axios.delete('/app/persons/' + this.$data.id)
            //     .then(response => console.log(response))
            //     .catch(error => console.log(error));
        },

        store() {
            // axios.post('/app/persons/', {
            // })
            //     .then(response => console.log(response))
            //     .catch(error => console.log(error));
        },

        update() {
            // axios.patch('/app/persons/' + this.$data.id, {
            // })
            //     .then(response => console.log(response))
            //     .catch(error => console.log(error));
        },

        loadData(data) {
            // data['phones'] = JSON.parse(data['phones']);
            // for(let field in data) {
            //     this.$data[field] = data[field];
            // }
        },
    },

    mounted() {
        console.log("Init dispatch script...");
        // if(person != null) {
        //     this.loadData(person);
        // }
    }
});