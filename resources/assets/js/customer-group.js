import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#customer-group',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        form: new Form({
            name: '',
            code: '',
        }),
    },

    methods: {

        destroy() {
            this.form.delete('/app/api/customer-groups/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Customer Group deleted!',
                        text: 'Customer Group removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/customer-groups');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.form.post('/app/api/customer-groups')
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Customer Group created!',
                        text: 'Customer Group added to database.',
                        type: 'success'
                    });
                    this.form.reset();
                })
                .catch(error => console.log(error));
        },

        update() {
            this.form.patch('/app/api/customer-groups/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.form.updated_by = response.updated_by;
                    this.form.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Customer Group updated!',
                        text: 'Changes saved to Customer Group database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.form = new Form({
                id: data.id,
                name: data.name,
                code: data.code,
                updated_by: data.updated_by,
            });
        },
    },

    mounted() {
        console.log("Init customer-group script...");
        if(customerGroup != null) {
            this.loadData(customerGroup);
        }
    }
});