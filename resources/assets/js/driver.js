import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#driver',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        driver: new Form({
            first_name: '',
            last_name: '',
            middle_name: '',
            notes: '',
        }),
    },

    methods: {

        destroy() {
            this.driver.delete('/app/drivers/' + this.driver.id)
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Driver deleted!',
                        text: 'Driver removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/drivers');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.driver.post('/app/drivers')
                .then(response => {
                    this.$swal({
                        title: 'Driver created!',
                        text: 'Driver added to database.',
                        type: 'success'
                    });
                    this.driver.reset();
                })
                .catch(error => console.log(error));
        },

        update() {
            this.driver.patch('/app/drivers/' + this.driver.id)
                .then(response => {
                    console.log(response);
                    this.driver.updated_by = response.updated_by;
                    this.driver.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Driver updated!',
                        text: 'Changes saved to driver database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.driver = new Form({
                id: data.id,
                last_name: data.last_name,
                middle_name: data.middle_name,
                first_name: data.first_name,
                notes: data.notes,
                updated_by: data.updated_by,
            });
        },
    },

    mounted() {
        console.log("Init driver script...");
        if(driver != null) {
            this.loadData(driver);
        }
    }
});