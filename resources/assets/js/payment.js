import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'

new Vue({
    el: '#payment',

    components: {
        SaveButton, DeleteButton
    },

    data: {
        payment: new Form({
            date: '',
            ref_no: '',
            amount: '',
            customer_id: null,
            statement_id: null,
        }),

        selectedCustomer: {},
        selectedStatement: {},

        statements: statements,
        customers: customers || [],
    },

    watch: {

        selectedStatement(val) {
            this.payment.statement_id = val.id;
        },

        selectedCustomer(val) {
            this.payment.customer_id = val.id;
            if (val.id == this.payment.originalData.customer_id) {
                this.payment.statement_id = this.payment.originalData.statement_id;
            } else {
                this.payment.statement_id = null;
            }
        },

    },

    computed: {

        paymentStatement() {
            return this.statements.find(statement => statement.id == this.payment.statement_id);
        },

        customerStatements() {
            return this.statements.filter(statement => statement.customer_id == this.selectedCustomer.id);
        },
    },

    methods: {

        destroy() {
            this.payment.patch('/app/payments/' + this.payment.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        store() {
            this.payment.post('/app/payments')
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        update() {
            this.payment.patch('/app/payments/' + this.payment.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.payment = new Form(data);
            this.selectedCustomer = this.customers.find(customer => data.customer_id == customer.id);
        },
    },

    mounted() {
        console.log("Init payment script...");
        if(payment != null) {
            this.loadData(payment);
        }
    }
});