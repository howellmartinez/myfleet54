import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#trip',

    components: {
        SaveButton, DeleteButton, Timestamp
    },

    data: {
        trip: new Form({
            ticket_no: '',
            completed: false,
            notes: '',

            passenger_id: '',
            passenger_contact: '',
            reservation_id: '',
            driver_id: '',
            car_id: '',

            date: '',
            type: '',
            location: '',
            destination: '',
            pickup_place: '',
            pickup_time: '',

            petty_cash: 0.00,

            legs: [],
        }),

        selectedReservation: {},

        trip_types: trip_types,
        locations: locations,

        drivers: drivers || [],
        cars: cars || [],
        reservations: reservations || [],
        persons: persons || [],
    },

    methods: {

        addLeg(trip) {
            trip.legs.push({
                order: 1,
                origin: '',
                destination: '',
                datetime_start: '',
                datetime_end: '',
                km_start: '',
                km_end: '',
                fuel_start: '',
                fuel_end: '',
            });
        },

        reservationChanged(reservation) {
            console.log('Reservation changed...');
            this.trip.date = reservation.trip_date;
            this.trip.type = reservation.type;
            this.trip.location = reservation.location;
            this.trip.destination = reservation.destination;
            this.trip.pickup_place = reservation.pickup_place;
            this.trip.pickup_time = reservation.pickup_time;
            this.trip.passenger_id = reservation.passenger_id;
            this.trip.reservation_id = reservation.id;
        },

        destroy() {
            this.trip.delete('/app/trips/' + this.trip.id)
                .then(response => {
                    console.log(response);
                    window.location = response.data.url;
                }).catch(error => console.log(error));
        },

        store() {
            this.trip.post('/app/trips/')
                .then(response => {
                    console.log(response);
                    window.location = response.data.url;
                }).catch(error => console.log(error));
        },

        update() {
            console.log('Updating...');
            this.trip.patch('/app/trips/' + this.trip.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            console.log('Loading data...');
            this.trip = new Form(data);
            this.selectedReservation = this.reservations.find(reservation => this.trip.reservation_id == reservation.id);
        },
    },

    mounted() {
        console.log("Init trip script...");
        if (trip != null) {
            this.loadData(trip);
        }
    }
});