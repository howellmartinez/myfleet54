import {Errors} from './Error';
import swal from 'sweetalert2';

export class Form {

    constructor(data) {
        this.isBusy = false;
        this.isSaving = false;
        this.isDeleting = false;
        this.originalData = data;
        for (let field in data) {
            // Deep copy if array
            if (Array.isArray(data[field])) {
                // console.log('Cloning array', field);
                this[field] = JSON.parse(JSON.stringify(data[field]));
            } else {
                this[field] = data[field];
            }
        }
        this.errors = new Errors();
    }

    data() {
        let data = {};
        for (let property in this.originalData) {
            data[property] = Array.isArray(this[property]) ? JSON.stringify(this[property]) : this[property];
        }
        return data;
    }

    reset() {
        for (let field in this.originalData) {
            this[field] = this.originalData[field];
        }
        this.errors.clear();
    }

    showAlert(text = '', title = 'Are you sure?', type = 'warning') {
        return swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
        });
    }

    check(url) {
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {

                    // Process the check results here
                    let message = '';
                    let data = response.data;
                    //
                    //     if (Object.keys(data).length > 0) {
                    //         // Send confirmation
                    //         let message = '';
                    //         for (let field in data) {
                    //             message = field;
                    //         }
                    //     }

                    let result = true;

                    if (result) {
                        resolve(this.showAlert(message));
                    }

                    resolve(true);
                }).catch(error => {
                    reject(error);
                });
        });

    }

    post(url) {
        this.isSaving = true;
        return this.submit('post', url);
    }

    put(url) {
        this.isSaving = true;
        return this.submit('put', url);
    }

    patch(url) {
        this.isSaving = true;
        return this.submit('patch', url);
    }

    delete(url) {
        return swal({
            title: 'Confirmation',
            text: 'Are you sure you want to delete this?',
            type: 'warning',
            showCancelButton: true,
        }).then(() => {
            this.isDeleting = true;
            return this.submit('delete', url);
        });
    }

    submit(requestType, url) {
        this.isBusy = true;
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data())
                .then(response => {
                    this.onSuccess(response.data);
                    resolve(response.data);
                }).catch(error => {
                    this.onFail(error.response.data);
                    reject(error.response.data);
                });
        });
    }

    onSuccess(data) {
        this.isBusy = false;
        this.isDeleting = false;
        this.isSaving = false;
    }

    onFail(errors) {
        this.errors.record(errors);
        if(this.errors.has('form')){
            swal({
                title: 'Oops!',
                text: this.errors.get('form'),
                type: 'error'
            }).then(
                () => this.errors.clear('form'),
                (dismiss) => this.errors.clear('form'));
        }
        this.isBusy = false;
        this.isDeleting = false;
        this.isSaving = false;
    }
}