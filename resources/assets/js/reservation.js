import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#reservation',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        reservation: new Form({
            'ref_no': '',
            'completed': '',
            'date': moment().format('YYYY-MM-DD'),
            'department': '',
            'purpose': '',
            'charging_loc': '',
            'trip_date': '',
            'location': '',
            'destination': '',
            'pickup_place': '',
            'pickup_time': '',
            'vehicle': '',
            'trip_end': '',
            'notes': '',
            'po_document_path': '',
            'trip_type_id': '',
            'customer_id': '',
            'requestor_id': '',
            'authorizer_id': '',
            'passenger_id': '',
            'passenger_contact': '',
        }),

        selectedPassenger: {},
        selectedCustomer: {},

        persons: persons,
        customers: customers,
        tripTypes: tripTypes,
    },

    watch: {
        selectedPassenger(val) {
            this.reservation.passenger_id = val.id
        },

        selectedCustomer(val) {
            this.reservation.customer_id = val.id
        },

    },

    computed: {
        customerPersons() {
            return this.persons.filter(person => person.customer_id == this.reservation.customer_id);
        }
    },

    methods: {

        destroy() {
            this.reservation.delete('/app/reservations/' + this.reservation.id)
                .then(response => {
                    this.$swal({
                        title: 'Reservation deleted!',
                        text: 'Reservation removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/reservations');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.reservation.post('/app/reservations')
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Reservation created!',
                        text: 'Reservation added to database.',
                        type: 'success'
                    });
                    this.reservation.reset();
                })
                .catch(error => console.log(error));
        },

        update() {
            this.reservation.patch('/app/reservations/' + this.reservation.id)
                .then(response => {
                    console.log(response);
                    this.reservation.updated_by = response.updated_by;
                    this.reservation.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Reservation updated!',
                        text: 'Changes saved to reservations database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            console.log('Loading data...');
            console.log();
            // data.trip_start = moment(data.trip_start).format('Y-MM-DD\THH:mm');
            // data.trip_end = moment(data.trip_end).format('Y-MM-DD\THH:mm');
            this.reservation = new Form(data);
            this.selectedPassenger = this.persons.find(person => data.passenger_id == person.id);
        },
    },

    mounted() {
        console.log("Init reservation script...");
        if(reservation != null) {
            this.loadData(reservation);
        }
    }
});