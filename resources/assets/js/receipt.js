import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'

new Vue({
    el: '#receipt',
    components: {
        SaveButton, DeleteButton
    },
    data: {
        receipt: new Form({
            date: '',
            ref_no: '',
            total: 0.00,
            customer_id: null,
            payments: [],
        }),

        selectedCustomer: {},

        payments: payments || [],
        customers: customers || [],
    },

    watch: {
        selectedCustomer(val) {
            this.receipt.customer_id = val.id;
            if (val.id == this.receipt.originalData.customer_id) {
                this.receipt.payments = this.receipt.originalData.payments;
            } else {
                this.receipt.payments = [];
            }
        },

        total(val) {
            this.receipt.total = val;
        },
    },

    computed: {

        customerPayments() {
            return this.payments.filter(payment => payment.customer_id == this.selectedCustomer.id);
        },

        receiptPayments() {
            return this.payments.filter(payment => this.receipt.payments.includes(payment.id));
        },

        total() {
            return this.receiptPayments.reduce((prev, curr) => prev + Number(curr.amount), 0.00);
        },


    },

    methods: {

        destroy() {
            this.receipt.delete('/app/receipts/' + this.receipt.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        store() {
            this.receipt.post('/app/receipts')
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        update() {
            this.receipt.patch('/app/receipts/' + this.receipt.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.receipt = new Form({
                id: data.id,
                ref_no: data.ref_no,
                customer_id: data.customer_id,
                date: data.date,
                total: data.total,
                updated_by: data.updated_by,
                payments: data.payments.map(payment => payment.id), // extract ids
            });

            this.selectedCustomer = this.customers.find(customer => data.customer_id == customer.id);
        },
    },

    mounted() {
        console.log("Init receipt script...");
        if(receipt != null) {
            this.loadData(receipt);
        }
    }
});