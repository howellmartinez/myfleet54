import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'

new Vue({
    el: '#transmittal',

    components: {
        SaveButton, DeleteButton
    },

    data: {
        transmittal: new Form({
            date: '',
            ref_no: '',
            customer_id: '',
            total: 0.00,
            statements: [],
        }),

        statements: statements || [],

        selectedCustomer: {},

        customers: customers || [],
    },

    watch: {
        selectedCustomer(val) {
            this.transmittal.customer_id = val.id;
            if (val.id == this.transmittal.originalData.customer_id) {
                this.transmittal.statements = this.transmittal.originalData.statements;
            } else {
                this.transmittal.statements = [];
            }
        },

        total(val) {
            this.transmittal.total = val;
        },
    },

    computed: {

        customerStatements() {
            return this.statements.filter(statement => statement.customer_id == this.selectedCustomer.id);
        },

        transmittalStatements() {
            return this.statements.filter(statement => this.transmittal.statements.includes(statement.id));
        },

        total() {
            return this.transmittalStatements.reduce((prev, curr) => prev + Number(curr.total), 0.00);
        },
    },

    methods: {

        destroy() {
            this.transmittal.patch('/app/transmittals/' + this.transmittal.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        store() {
            this.transmittal.post('/app/statements')
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        update() {
            this.transmittal.patch('/app/transmittals/' + this.transmittal.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.transmittal = new Form({
                id: data.id,
                ref_no: data.ref_no,
                customer_id: data.customer_id,
                date: data.date,
                total: data.total,
                updated_by: data.updated_by,
                statements: data.statements.map(statement => statement.id), // extract ids
            });

            this.selectedCustomer = this.customers.find(customer => data.customer_id == customer.id);
        },
    },

    mounted() {
        console.log("Init transmittal script...");
        if(transmittal != null) {
            this.loadData(transmittal);
        }
    }
});