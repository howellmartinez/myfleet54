import {Form} from './components/Form'

import FieldList from './components/FieldList'
import ErrorMessage from './components/ErrorMessage'
import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#person',

    components: {
        FieldList, ErrorMessage, SaveButton, DeleteButton, Timestamp
    },

    data: {
        form: new Form({
            last_name: '',
            first_name: '',
            middle_name: '',
            nickname: '',
            notes: '',
            phones: [{label: '', value: ''}],
            emails: [{label: '', value: ''}],
            relationships: [{label: '', value: ''}],
            others: [{label: '', value: ''}],

            drivers: [],
            customers: [],
        }),

        customers: customers,

        tabs: [],

        newTabType: '',

        tabTypes: ['Customer', 'Driver'],

        currentTab: 'General',
    },

    methods: {

        addCustomer() {
            this.form.customers.push({
                pivot: {
                    customer_id: '',
                    position: '',
                    department: '',
                },

                // primary: true,
            });
        },

        addDriver() {
            this.form.drivers.push({
                notes: '',
            });
        },

        addTab() {
            switch (this.newTabType) {
                case 'Customer': {
                    this.addCustomer();
                    this.currentTab = 'Customer ' + this.form.customers.length;
                    break;
                }

                case 'Driver': {
                    this.addDriver();
                    this.currentTab = 'Driver ' + this.form.drivers.length;
                    break;
                }
                default:
                    break;
            }
        },

        store() {
            this.form.post('/app/persons')
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Person created!',
                        text: this.form.first_name + ' ' + this.form.last_name +  ' added to person database.',
                        type: 'success'
                    });
                    this.form.reset();
                }).catch(errors => console.log(errors));
        },

        update() {
            this.form.patch('/app/persons/' + this.form.id)
                .then(response => {
                    console.log(response);
                    this.form.updated_by = response.updated_by;
                    this.form.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Person updated!',
                        text: 'Changes saved to person database.',
                        type: 'success'
                    });
                }).catch(errors => console.log(errors));
        },

        destroy() {
            this.form.delete('/app/persons/' + this.form.id)
                .then(data => {
                    this.$swal({
                        title: 'Person deleted!',
                        text: 'Person removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/persons');
                }).catch(errors => console.log(errors));
        },

        loadData(data) {
            console.log("Loading person data...");
            this.form = new Form({
                id: data.id,
                last_name: data.last_name,
                first_name: data.first_name,
                middle_name: data.middle_name,
                nickname: data.nickname,
                notes: data.notes,
                phones: JSON.parse(data['phones']),
                emails: JSON.parse(data['emails']),
                relationships: JSON.parse(data['emails']),
                others: JSON.parse(data['others']),
                customers: data['customers'],
                drivers: data['drivers'],
                updated_by: data.updated_by,
                updated_at: data.updated_at,
                created_at: data.created_at,
            });
        },

    },

    mounted() {
        console.log("Init person script...");
        if (person != null) {
            this.loadData(person);
        }
    }
});