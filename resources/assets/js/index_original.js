import Filters from './components/Filters'
import PaginationLinks from './components/PaginationLinks'

new Vue({
    el: '#index',

    components: { Filters, PaginationLinks },

    data: {
        isLoading: false,
        filterableFields: filterableFields,
        conditions: [],
        items: [],
        pagination: {},
        baseUrl: url,
        page: 1,
    },

    computed: {
        conditionString() {
            return this.conditions.reduce((prev, curr) => {
                return prev.concat('&' + curr.field + '=' + curr.value)
            }, '');
        },

        assembledUrl() {
            return this.baseUrl + '?page=' + this.page + this.conditionString;
        },

        noItems() {
            return this.items.length == 0;
        },

        isLastPage() {
            return this.page == this.pagination.last_page;
        },

        isFirstPage() {
            return this.page == 1;
        },

        paginationStatus() {
            if (this.isLoading) return '';
            return 'Displaying items ' + this.pagination.from + ' to ' + this.pagination.to + ' of ' + this.pagination.total;
        },
    },

    methods: {

        addCondition() {
            this.conditions.push({
                field: '',
                value: '',
            })
        },

        filter() {
            this.load(this.assembledUrl);
        },

        clear() {
            this.conditions = [];
            this.page = 1;
            this.load(this.assembledUrl);
        },

        load(url) {
            this.isLoading = true;
            axios.get(url)
                .then(this.onSuccess)
                .catch(this.onError);
        },

        onSuccess(response) {
            // console.log(response.data);
            this.pagination = response.data;
            this.items = response.data.data;
            this.isLoading = false;
        },

        onError(error) {
            console.log(error);
            this.isLoading = false;
        },

        prevPage() {
            if (this.isFirstPage || this.isLoading) return;
            this.page--;
            this.load(this.assembledUrl);
        },

        nextPage() {
            if (this.isLastPage || this.isLoading) return;
            this.page++;
            this.load(this.assembledUrl);
        },

    },

    mounted() {
        this.load(this.baseUrl);
    }
});