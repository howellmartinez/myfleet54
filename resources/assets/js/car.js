import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#car',
    components: {
        SaveButton, DeleteButton, Timestamp
    },
    data: {
        car: new Form({
            'type': '',
            'class': '',
            'make': '',
            'model': '',
            'fuel': 'Gas',
            'tx': 'MT',
            'year': '',
            'color': '',
            'max_cap': 1,
            'plate': '',
            'conduction_no': '',
            'engine_no': '',
            'serial_no': '',
            'or_no': '',
            'cr_no': '',
            'registered_owner': '',
            'status': 'Active',
        }),

        classifications: classifications,
    },

    computed: {
        carType() {
            if (this.car.class == '') return '';
            return this.classifications.find(classification => classification.classes.includes(this.car.class)).type;
        }
    },

    watch: {
        carType(val) {
            this.car.type = val;
        }
    },

    methods: {

        destroy() {
            this.car.delete('/app/cars/' + this.car.id)
                .then(response => {
                    console.log(response);
                    this.$swal({
                        title: 'Car deleted!',
                        text: 'Car removed from database.',
                        type: 'success'
                    }).then(() => window.location = '/app/cars');
                })
                .catch(error => console.log(error));
        },

        store() {
            this.car.post('/app/cars')
                .then(response => {
                    // console.log(response);
                    this.$swal({
                        title: 'Car created!',
                        text: 'Car added to database.',
                        type: 'success'
                    });
                    this.car.reset();
                })
                .catch(error => console.log(error));
        },

        update() {
            this.car.patch('/app/cars/' + this.car.id)
                .then(response => {
                    console.log(response);
                    this.car.updated_by = response.updated_by;
                    this.car.updated_at = response.updated_at;
                    this.$swal({
                        title: 'Car updated!',
                        text: 'Changes saved to car database.',
                        type: 'success'
                    });
                })
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.car = new Form(data);
        },
    },

    mounted() {
        console.log("Init car script...");
        if(car != null) {
            this.loadData(car);
        }
    }
});