import SaveButton from './components/SaveButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#permissions',

    components: {
        SaveButton, Timestamp
    },

    data: {
        permissionGroups: permissionGroups,
        users: [],
        icons: {
            'create': 'fa-plus',
            'edit': 'fa-pencil',
            'delete': 'fa-trash',
            'view': 'fa-eye',
        },
        isSaving: false,
    },

    computed: {
        payload() {
            return this.users;
        }
    },

    methods: {
        save() {
            this.isSaving = true;
            axios.post('/app/api/users/permissions', this.payload)
                .then((response) => {
                    this.isSaving = false;
                    console.log(response);
                }).catch();
        }
    },

    mounted() {
        this.users = users.map(user => {
            user.permission_names = user.permissions.map(permission => permission.name);
            return user;
        })
    },

});