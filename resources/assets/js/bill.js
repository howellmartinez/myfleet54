import {Form} from './components/Form';

import SaveButton from './components/SaveButton'
import DeleteButton from './components/DeleteButton'
import Timestamp from './components/Timestamp'

new Vue({
    el: '#bill',

    components: {
        SaveButton, DeleteButton, Timestamp
    },

    data: {
        bill: new Form({
            ref_no: '',
            date: '',
            trip_id: null,
            regular_fare: 0.00,
            excess_hours_charge: 0.00,
            driver_ot_charge: 0.00,
            vat: 0.00,
            fuel_charge: 0.00,
            meals: 0.00,
            tolls: 0.00,
            parking: 0.00,
            lodging: 0.00,
            others: 0.00,
            approved_by: '',

            vatable_total: 0.00,
            service_total: 0.00,
            reimbursement_total: 0.00,
            total: 0.00,
        }),
        selectedTrip: {},
        trips: trips || [],
    },

    watch: {

        selectedTrip(val) {
            this.bill.trip_id = val.id;
        },

        total(val) {
            this.bill.total = val;
        },

        reimbursementTotal(val) {
            this.bill.reimbursement_total = val;
        },

        vatableTotal(val) {
            this.bill.vatable_total = val;
        },

        serviceTotal(val) {
            this.bill.service_total = val;
        },

    },

    computed: {

        total() {
            return this.serviceTotal + this.reimbursementTotal;
        },

        reimbursementTotal() {
            return this.bill.fuel_charge + this.bill.meals + this.bill.tolls + this.bill.parking + this.bill.lodging + this.bill.others;
        },

        vatableTotal() {
            return this.bill.regular_fare + this.bill.excess_hours_charge + this.bill.driver_ot_charge;
        },

        serviceTotal() {
            return this.vatableTotal + this.bill.vat;
        },

        car() {
            return this.selectedTrip.hasOwnProperty('car') ? this.selectedTrip.car : null;
        },

        passenger() {
            return this.selectedTrip.hasOwnProperty('passenger') ? this.selectedTrip.passenger : null;
        },

        reservation() {
            return this.selectedTrip.hasOwnProperty('reservation') ? this.selectedTrip.reservation : null;
        },

        authorizer() {
            return this.reservation ? this.reservation.authorizer : null;
        },

        requestor() {
            return this.reservation ? this.reservation.requestor : null;
        },

        customer() {
            return this.reservation ? this.reservation.customer : null;
        },
    },

    methods: {

        // getNumeric(value) {
        //     return Number(value.replace(/,/g, ''));
        // },

        destroy() {
            this.bill.delete('/app/bills/' + this.bill.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        store() {
            this.bill.post('/app/bills/')
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        update() {
            this.bill.patch('/app/bills/' + this.bill.id)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        },

        loadData(data) {
            this.bill = new Form({
                id: data.id,
                trip_id: data.trip_id,
                regular_fare: Number(data.regular_fare),
                excess_hours_charge: Number(data.excess_hours_charge),
                driver_ot_charge: Number(data.driver_ot_charge),
                vat: Number(data.vat),
                fuel_charge: Number(data.fuel_charge),
                meals: Number(data.meals),
                tolls: Number(data.tolls),
                parking: Number(data.parking),
                lodging: Number(data.lodging),
                others: Number(data.others),
                approved_by: data.approved_by,

                vatable_total: Number(data.vatable_total),
                service_total: Number(data.service_total),
                reimbursement_total: Number(data.reimbursement_total),
                total: Number(data.total),
            });

            this.selectedTrip = this.trips.find(trip => data.trip_id == trip.id);
        },
    },

    mounted() {
        console.log("Init bill script...");
        if(bill != null) {
            this.loadData(bill);
        }
    }
});